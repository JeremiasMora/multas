package com.example.registroPersona;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.google.gson.Gson;


import com.example.registroPersona.Persona;
import com.example.registrologin.R;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	 private static final String METHOD_NAME = "crearPersonaAndroid";
     private static final String NAMESPACE = "http://webservice";
     private static final String SOAP_ACTION = "http://webservice/crearPersonaAndroid";
     private static final String URL = "http://192.168.1.133:8080/MultaProyect2/services/ServicioPersona?wsdl";


     String textResponseServidor;
    
     TextView textResp, tvNombre,tvApellido,tvRut, tvPass, tvDireccion;
     EditText nombre,apellido, rut,pass, direccion;
     Button bEnviar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		tvNombre = (TextView)findViewById(R.id.Nombre);
		tvApellido = (TextView)findViewById(R.id.Apellido);
		tvRut = (TextView)findViewById(R.id.Rut);
		tvPass = (TextView)findViewById(R.id.Pass);
		tvDireccion = (TextView)findViewById(R.id.Direccion);
		
        nombre=(EditText) findViewById(R.id.editText1);
        apellido=(EditText) findViewById(R.id.editText2);
        rut=(EditText) findViewById(R.id.editText7);
        pass=(EditText) findViewById(R.id.editText5);
        direccion=(EditText) findViewById(R.id.editText4);
        bEnviar=(Button) findViewById(R.id.button1);
        
        bEnviar.setOnClickListener(new OnClickListener() {
            
            public void onClick(View v) {
                    // TODO Auto-generated method stub
                    String resp=registrarPersona(nombre.getText().toString(),
                    		apellido.getText().toString(),
                       		rut.getText().toString(),
                    		pass.getText().toString(),
                    		direccion.getText().toString());
                    textResp.setText(resp);
                   
            }
    });

		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	 public String registrarPersona(String nombre, String apellido, String rut,String pass, String direccion){
         
         SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
         //GSON
         Persona persona = new Persona();
         persona.setNombre(nombre);
         persona.setApellido(apellido);
         persona.setRut(rut);
         persona.setDireccion(direccion);
         persona.setPass(pass);
         final Gson gson = new Gson();
         String send = gson.toJson(persona);

         Log.i("JSON", send);
        
         //parametros
         PropertyInfo jSonPI=new PropertyInfo();
         jSonPI.setName("mensaje");
         jSonPI.setValue(send);
         jSonPI.setType(String.class);
         request.addProperty(jSonPI);

         //Envio
         new MensajeSoap().execute(request);

         return null;
    }

    public void mostrarAlerta(){
        new AlertDialog.Builder(this)
                .setTitle("Listo")
                .setMessage(this.textResponseServidor)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();


    }

    class MensajeSoap extends AsyncTask<SoapObject, Void, String> {

        private Exception exception;

        protected String doInBackground(SoapObject... primitives) {

            SoapObject object = primitives[0];

            if(object == null){
                Log.e("JSON", "objeto nulo");
            }

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(object);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug=true;

            try {
                Log.d("Webservice Output","Iniciando envio");
                androidHttpTransport.call(SOAP_ACTION, envelope);
                SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
                Log.i("Webservice Output", response.toString());

                return response.toString();

            } catch (Exception e) {
                e.printStackTrace();
                Log.i("Catchhh", " " + e);

                return "error";
            }

        }

        protected void onPostExecute(String response) {
            MainActivity.this.textResponseServidor = response;
            MainActivity.this.mostrarAlerta();
        }
    }
}
