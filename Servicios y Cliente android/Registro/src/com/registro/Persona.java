package com.registro;

public class Persona {

	private String rut;
	private String pass;
	private String nombre;
	private String apellido;
	private String direccion;

	public Persona (){	
	}
	
	public Persona (String rut, String pass, String nombre, String apellido, String direccion){
		this.rut =rut;
		this.pass= pass;
		this.apellido=apellido;
		this.nombre = nombre;
		this.direccion = direccion;
	}
	



	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
}

