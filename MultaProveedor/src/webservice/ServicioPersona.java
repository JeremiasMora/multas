package webservice;

import java.util.ArrayList;
import java.util.List;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;


// TODO: Auto-generated Javadoc
/**
 * The Class ServicioPersona.
 */
public class ServicioPersona {
	
	/**
	 * Crear persona.
	 *
	 * @param p the p
	 * @return the string
	 */
	public String crearPersona(domain.Persona p){
		PersistentTransaction t;
		try{
			t = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().beginTransaction();
			try {
				orm.Persona lormPersona = orm.PersonaDAO.createPersona();

				lormPersona.setNombre(p.getNombre());
				lormPersona.setApellido(p.getApellido());
				lormPersona.setPass(p.getPass());
				lormPersona.setDireccion(p.getDireccion());
				lormPersona.setRut(p.getRut());
				orm.PersonaDAO.save(lormPersona);
				t.commit();
				return " Persona ingresada";
				
			} catch (Exception e) {
				// TODO: handle exception
				t.rollback();
				return "Error rollback";
			}
		}catch(PersistentException e){
			e.printStackTrace();
			return "Error persistencia";
		}
	}
	
	
	/**
	 * Loguearse.
	 *
	 * @param user the user
	 * @return true, if successful
	 */
	public boolean loguearse(domain.LoginVO user) {
		
		return new negocio.Usuario().loguearse(user);

	}


	/**
	 * Mostrar persona.
	 *
	 * @param p the p
	 * @return the list
	 */
	public List<domain.Persona> mostrarPersona(domain.Persona p){
		List<domain.Persona> personaIngresado = new ArrayList<domain.Persona>(); 
		  
        PersistentTransaction t; 
        try { 
            orm.Persona[] personas = orm.PersonaDAO.listPersonaByQuery("id='1'", null);
            t = orm.DiagramadelaBDMULTASPersistentManager 
                    .instance().getSession().beginTransaction(); 
  
            for (int i = 0; i < personas.length; i++) { 
                personaIngresado.add(new domain.Persona(personas[i].getRut(), personas[i] 
                        .getPass(), personas[i].getNombre(), personas[i].getApellido(), 
                        personas[i].getDireccion())); }	        
        
        return personaIngresado; 
    } catch (PersistentException e) { 
        // TODO Auto-generated catch block 
        e.printStackTrace(); 
        return null; 
    } 
	}
}