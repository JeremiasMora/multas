package webservice;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

// TODO: Auto-generated Javadoc
/**
 * The Class ServicioInfractor.
 */
public class ServicioInfractor {
	
	/**
	 * Crear infractor.
	 *
	 * @param p the p
	 * @return the string
	 */
	public String crearInfractor(domain.Persona p){
		PersistentTransaction t;
		try{
			t = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().beginTransaction();
			try {
				
				orm.Persona lormPersona = orm.PersonaDAO.createPersona();
				
				lormPersona.setNombre(p.getNombre());
				lormPersona.setApellido(p.getApellido());
				lormPersona.setPass(p.getPass());
				lormPersona.setDireccion(p.getDireccion());
				lormPersona.setRut(p.getRut());
				orm.PersonaDAO.save(lormPersona);
				
				orm.Infractor_persona lormInfractor = orm.Infractor_personaDAO.createInfractor_persona();
					
				lormInfractor.setPersonaid_usuario(lormPersona);
				orm.Infractor_personaDAO.save(lormInfractor);
				t.commit();
					return ""+lormInfractor.getORMID();  
				
				
			} catch (Exception e) {
				// TODO: handle exception
				t.rollback();
				return "Error rollback";
			}
		}catch(PersistentException e){
			e.printStackTrace();
			return "Error persistencia";
		}
	}
	/*
	public static void main(String[] args) {
		domain.Persona p = new domain.Persona();
		String resultado = new ServicioInfractor().crearInfractor(p);
		System.out.println(resultado);
	}
	*/
}