package webservice;

import java.util.ArrayList;
import java.util.List;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

// TODO: Auto-generated Javadoc
/**
 * The Class ServicioCarabinero.
 */
public class ServicioCarabinero {
	
	/**
	 * Crear carabinero.
	 *
	 * @param p the p
	 * @return the string
	 */
	public String crearCarabinero(domain.Persona p){
		PersistentTransaction t;
		try{
			t = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().beginTransaction();
			try {
				
				orm.Persona lormPersona = orm.PersonaDAO.createPersona();
				
				lormPersona.setNombre("asd");
				lormPersona.setApellido("asd");
				lormPersona.setPass("asd");
				lormPersona.setDireccion("asd");
				lormPersona.setRut("asd");
				orm.PersonaDAO.save(lormPersona);
				
				orm.Carabinero lormCarabinero = orm.CarabineroDAO.createCarabinero();
					
				lormCarabinero.setPersonaid_usuario(lormPersona);
				orm.CarabineroDAO.save(lormCarabinero);
				t.commit();
					return " Carabinero creado";  
				
				
			} catch (Exception e) {
				// TODO: handle exception
				t.rollback();
				return "Error rollback";
			}
		}catch(PersistentException e){
			e.printStackTrace();
			return "Error persistencia";
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		domain.Persona p = new domain.Persona();
		String resultado = new ServicioCarabinero().crearCarabinero(p);
		System.out.println(resultado);
	}
	
}