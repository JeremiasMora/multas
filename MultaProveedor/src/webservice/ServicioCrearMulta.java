package webservice;

import java.util.ArrayList;
import java.util.List;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;


// TODO: Auto-generated Javadoc
/**
 * The Class ServicioCrearMulta.
 */
public class ServicioCrearMulta {
	
	/**
	 * Crear multa.
	 *
	 * @param m the m
	 * @param idInfractor the id infractor
	 * @return the string
	 */
	public String crearMulta(domain.Multa m, int idInfractor){
		PersistentTransaction t;
		try{
			t = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().beginTransaction();
			try {
				orm.Carabinero lormCarabinero = orm.CarabineroDAO.loadCarabineroByQuery("id='1'", null);
				orm.Multa lormMulta = orm.MultaDAO.createMulta();
				
				lormMulta.setNombre_multa(m.getNombre_multa());
				lormMulta.setDescripcion_multa(m.getDescripcion_multa());
				lormMulta.setFecha_creacion(m.getFecha_creacion());
				lormMulta.setQuien_modifico_multa(m.getQuien_modifico_multa());
				lormMulta.setQuien_registro_multa(m.getQuien_registro_multa());
				
				lormMulta.setCarabinero(lormCarabinero);
				orm.MultaDAO.save(lormMulta);

				orm.Infractor_persona_multa lormInfractorPersonaMulta = orm.Infractor_persona_multaDAO.createInfractor_persona_multa(); //se crea la tabla infractor_persona_multa
				orm.Infractor_persona infractor = orm.Infractor_personaDAO.getInfractor_personaByORMID(idInfractor); //Se busca un infractor por la ID (parametro de entrada)
				if(infractor!=null){
					lormInfractorPersonaMulta.setMulta(lormMulta);//Se asigna la multa del infractor
					lormInfractorPersonaMulta.setInfractor_persona(infractor); //Se asigna la ID del infractor
					orm.Infractor_persona_multaDAO.save(lormInfractorPersonaMulta); 
					t.commit();
					return " Persona Multada";
				}
				else{
					
					return "ID infractor no existe" + idInfractor;
				}
				
				
			} catch (Exception e) {
				// TODO: handle exception
				t.rollback();
				return "Error rollback";
			}
		}catch(PersistentException e){
			e.printStackTrace();
			return "Error persistencia";
		}
	}
	
	/**
	 * Mostrar multa.
	 *
	 * @param m the m
	 * @return the list
	 */
	public List<domain.Multa> mostrarMulta(domain.Multa m) {
		List<domain.Multa> multaIngresada = new ArrayList<domain.Multa>();

		PersistentTransaction t;
		try {
			orm.Multa[] multas = orm.MultaDAO.listMultaByQuery(null, null);
			t = orm.DiagramadelaBDMULTASPersistentManager.instance()
					.getSession().beginTransaction();

			for (int i = 0; i < multas.length; i++) {

				multaIngresada
						.add(new domain.Multa(multas[i].getNombre_multa(),
								multas[i].getDescripcion_multa(), multas[i]
										.getQuien_registro_multa(), multas[i]
										.getQuien_modifico_multa(), multas[i]
										.getFecha_creacion(), multas[i]
										.getDesactivar()));
			}

			return multaIngresada;
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}