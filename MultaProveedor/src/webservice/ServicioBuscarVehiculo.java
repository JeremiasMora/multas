package webservice;

import java.util.ArrayList;
import java.util.List;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import orm.Persona;
import domain.Vehiculos;

// TODO: Auto-generated Javadoc
/**
 * The Class ServicioBuscarVehiculo.
 */
public class ServicioBuscarVehiculo {
	
	/**
	 * Mostrar vehiculo.
	 *
	 * @param patente the patente
	 * @return the domain. vehiculos
	 */
	public domain.Vehiculos mostrarVehiculo(String patente){
		System.out.print("VEHICULO"+ patente);
		
		domain.Vehiculos vehiculosIngresado = new domain.Vehiculos(); 
        PersistentTransaction t; 
        try { 
           t = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().beginTransaction(); 
           orm.Vehiculos vehiculo = orm.VehiculosDAO.loadVehiculosByQuery("patente = '" + patente+ "'", null);
           
           if(vehiculo == null){
        	   System.out.print("La patente no existe");
        	   
           }else{
        	   System.out.print("Patente existe");
        	   vehiculosIngresado.setAnio(vehiculo.getAnio());
        	   vehiculosIngresado.setMarca(vehiculo.getMarca());
        	   vehiculosIngresado.setModelo(vehiculo.getModelo());
        	   vehiculosIngresado.setPatente(vehiculo.getPatente());
        	   vehiculosIngresado.setNombre_duenio(vehiculo.getPersonaid_usuario().getNombre() + " " + vehiculo.getPersonaid_usuario().getApellido());
        	   vehiculosIngresado.setRut_duenio(vehiculo.getPersonaid_usuario().getRut());
        	   
        	         	   
           }        
          
        return vehiculosIngresado; 
    } catch (PersistentException e) { 
        // TODO Auto-generated catch block 
        e.printStackTrace(); 
        return null; 
    } 
	}
}