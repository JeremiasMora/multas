package webservice;

import java.util.ArrayList;
import java.util.List;

import javax.swing.text.StyledEditorKit.BoldAction;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import orm.Infractor_persona_multa;

// TODO: Auto-generated Javadoc
/**
 * The Class ServicioBuscarInfractor.
 */
public class ServicioBuscarInfractor {

/**
 * Return id Infractor.
 *
 * @param p the p
 * @return the int
 */
	public int buscarInfractor(domain.Persona p) {
		int bandera = 0;
		PersistentTransaction t;
		
		try {
			t = orm.DiagramadelaBDMULTASPersistentManager.instance()
					.getSession().beginTransaction();
			try {

				orm.Infractor_persona lormInfractor_persona = orm.Infractor_personaDAO
						.loadInfractor_personaByQuery("personaid_usuario.rut='" + p.getRut() + "' ", null);

				if (lormInfractor_persona == null) {
					System.out.print("El infractor no existe");

					lormInfractor_persona = orm.Infractor_personaDAO.createInfractor_persona(); //Se crea el infractor
					orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByQuery("rut='" + p.getRut() + "'", null); //Se busca si el rut pertenece a una persona

					if (lormPersona == null) {
						bandera = 0;
						System.out.print("La persona no existe");
					}
					else{
						
						lormInfractor_persona.setPersonaid_usuario(lormPersona);//asigno la la persona al infractor
						orm.Infractor_personaDAO.save(lormInfractor_persona);
						bandera = lormInfractor_persona.getORMID();//se guarda en la bandera el ID del infractor_Persona
					}
				} else {
					System.out.print("Infractor ya esta creado");

//					t.commit();
					bandera = lormInfractor_persona.getORMID();
				}

			} catch (Exception e) {
				// TODO: handle exception
				t.rollback();
				System.out.print("Error Rollback ");
			}
			
			
			
		} catch (PersistentException e) {
			e.printStackTrace();
			System.out.print("Error persistencia");
		}
		return bandera;
	}
}