package domain;

public class Multa {
	private String nombre_multa;
	private String descripcion_multa;
	private String quien_registro_multa;
	private String quien_modifico_multa;
	private String fecha_creacion;
	private char desacrivar;
	public Multa(String nombre_multa, String descripcion_multa,
			String quien_registro_multa, String quien_modifico_multa,
			String fecha_creacion, char desacrivar) {
		super();
		this.nombre_multa = nombre_multa;
		this.descripcion_multa = descripcion_multa;
		this.quien_registro_multa = quien_registro_multa;
		this.quien_modifico_multa = quien_modifico_multa;
		this.fecha_creacion = fecha_creacion;
		this.desacrivar = desacrivar;
	}
	public Multa() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getNombre_multa() {
		return nombre_multa;
	}
	public void setNombre_multa(String nombre_multa) {
		this.nombre_multa = nombre_multa;
	}
	public String getDescripcion_multa() {
		return descripcion_multa;
	}
	public void setDescripcion_multa(String descripcion_multa) {
		this.descripcion_multa = descripcion_multa;
	}
	public String getQuien_registro_multa() {
		return quien_registro_multa;
	}
	public void setQuien_registro_multa(String quien_registro_multa) {
		this.quien_registro_multa = quien_registro_multa;
	}
	public String getQuien_modifico_multa() {
		return quien_modifico_multa;
	}
	public void setQuien_modifico_multa(String quien_modifico_multa) {
		this.quien_modifico_multa = quien_modifico_multa;
	}
	public String getFecha_creacion() {
		return fecha_creacion;
	}
	public void setFecha_creacion(String fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
	public char getDesacrivar() {
		return desacrivar;
	}
	public void setDesacrivar(char desacrivar) {
		this.desacrivar = desacrivar;
	}
	
	
}
