package domain;

public class Solicitud_cambio_fecha {
	
	private String carta_solicitud;
	private String fecha;
	public Solicitud_cambio_fecha() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Solicitud_cambio_fecha(String carta_solicitud, String fecha) {
		super();
		this.carta_solicitud = carta_solicitud;
		this.fecha = fecha;
	}
	public String getCarta_solicitud() {
		return carta_solicitud;
	}
	public void setCarta_solicitud(String carta_solicitud) {
		this.carta_solicitud = carta_solicitud;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	

}
