package domain;

public class Vehiculos {

	private String marca;
	private String modelo;
	private String nombre_duenio;
	private int anio;
	private String patente;
	private String rut_duenio;
	
	public Vehiculos(String marca, String modelo, String nombre_duenio,
			int anio,String patente) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.nombre_duenio = nombre_duenio;
		this.anio = anio;
		this.patente = patente;
	}

	public Vehiculos() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getPatente() {
		return patente;
	}
	public void setPatente(String patente) {
		this.patente = patente;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getNombre_duenio() {
		return nombre_duenio;
	}
	public void setNombre_duenio(String nombre_duenio) {
		this.nombre_duenio = nombre_duenio;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}

	public String getRut_duenio() {
		return rut_duenio;
	}

	public void setRut_duenio(String rut_duenio) {
		this.rut_duenio = rut_duenio;
	}



	
	
	
}
