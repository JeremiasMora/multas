package domain;

public class LoginVO {
	
	private String rut;
	
	private String pass;
	
	public LoginVO() {
		super();
	}

	public LoginVO(String rut, String pass) {
		super();
		this.rut = rut;
		this.pass = pass;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	
	
	

}