/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PersonaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id_usuario;
	public final StringExpression rut;
	public final StringExpression pass;
	public final StringExpression nombre;
	public final StringExpression apellido;
	public final StringExpression direccion;
	
	public PersonaDetachedCriteria() {
		super(orm.Persona.class, orm.PersonaCriteria.class);
		id_usuario = new IntegerExpression("id_usuario", this.getDetachedCriteria());
		rut = new StringExpression("rut", this.getDetachedCriteria());
		pass = new StringExpression("pass", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		apellido = new StringExpression("apellido", this.getDetachedCriteria());
		direccion = new StringExpression("direccion", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.PersonaCriteria.class);
		id_usuario = new IntegerExpression("id_usuario", this.getDetachedCriteria());
		rut = new StringExpression("rut", this.getDetachedCriteria());
		pass = new StringExpression("pass", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		apellido = new StringExpression("apellido", this.getDetachedCriteria());
		direccion = new StringExpression("direccion", this.getDetachedCriteria());
	}
	
	public JuzgadoDetachedCriteria createJuzgadoCriteria() {
		return new JuzgadoDetachedCriteria(createCriteria("ORM_juzgado"));
	}
	
	public VehiculosDetachedCriteria createVehiculosCriteria() {
		return new VehiculosDetachedCriteria(createCriteria("ORM_vehiculos"));
	}
	
	public SecretariaDetachedCriteria createSecretariaCriteria() {
		return new SecretariaDetachedCriteria(createCriteria("secretaria"));
	}
	
	public CarabineroDetachedCriteria createCarabineroCriteria() {
		return new CarabineroDetachedCriteria(createCriteria("carabinero"));
	}
	
	public TokenDetachedCriteria createTokenCriteria() {
		return new TokenDetachedCriteria(createCriteria("token"));
	}
	
	public Infractor_personaDetachedCriteria createInfractor_personaCriteria() {
		return new Infractor_personaDetachedCriteria(createCriteria("infractor_persona"));
	}
	
	public TesoreraDetachedCriteria createTesoreraCriteria() {
		return new TesoreraDetachedCriteria(createCriteria("tesorera"));
	}
	
	public Persona uniquePersona(PersistentSession session) {
		return (Persona) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Persona[] listPersona(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Persona[]) list.toArray(new Persona[list.size()]);
	}
}

