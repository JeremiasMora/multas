/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class VehiculosCriteria extends AbstractORMCriteria {
	public final IntegerExpression id_vehiculo;
	public final StringExpression marca;
	public final StringExpression modelo;
	public final IntegerExpression anio;
	public final StringExpression patente;
	
	public VehiculosCriteria(Criteria criteria) {
		super(criteria);
		id_vehiculo = new IntegerExpression("id_vehiculo", this);
		marca = new StringExpression("marca", this);
		modelo = new StringExpression("modelo", this);
		anio = new IntegerExpression("anio", this);
		patente = new StringExpression("patente", this);
	}
	
	public VehiculosCriteria(PersistentSession session) {
		this(session.createCriteria(Vehiculos.class));
	}
	
	public VehiculosCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public PersonaCriteria createPersonaid_usuarioCriteria() {
		return new PersonaCriteria(createCriteria("personaid_usuario"));
	}
	
	public MultaCriteria createMultaCriteria() {
		return new MultaCriteria(createCriteria("ORM_multa"));
	}
	
	public MultavehiculoCriteria createMultavehiculoCriteria() {
		return new MultavehiculoCriteria(createCriteria("ORM_multavehiculo"));
	}
	
	public Vehiculos uniqueVehiculos() {
		return (Vehiculos) super.uniqueResult();
	}
	
	public Vehiculos[] listVehiculos() {
		java.util.List list = super.list();
		return (Vehiculos[]) list.toArray(new Vehiculos[list.size()]);
	}
}

