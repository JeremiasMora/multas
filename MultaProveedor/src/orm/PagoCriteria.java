/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PagoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression formapago;
	
	public PagoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		formapago = new StringExpression("formapago", this);
	}
	
	public PagoCriteria(PersistentSession session) {
		this(session.createCriteria(Pago.class));
	}
	
	public PagoCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public TesoreraCriteria createTesoreraCriteria() {
		return new TesoreraCriteria(createCriteria("tesorera"));
	}
	
	public MultaCriteria createMultaCriteria() {
		return new MultaCriteria(createCriteria("multa"));
	}
	
	public Pago uniquePago() {
		return (Pago) super.uniqueResult();
	}
	
	public Pago[] listPago() {
		java.util.List list = super.list();
		return (Pago[]) list.toArray(new Pago[list.size()]);
	}
}

