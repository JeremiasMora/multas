/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class MultaDAO {
	public static Multa loadMultaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadMultaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa getMultaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getMultaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa loadMultaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadMultaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa getMultaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getMultaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa loadMultaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Multa) session.load(orm.Multa.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa getMultaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Multa) session.get(orm.Multa.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa loadMultaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Multa) session.load(orm.Multa.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa getMultaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Multa) session.get(orm.Multa.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMulta(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return queryMulta(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMulta(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return queryMulta(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa[] listMultaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listMultaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa[] listMultaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listMultaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMulta(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Multa as Multa");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMulta(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Multa as Multa");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Multa", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa[] listMultaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryMulta(session, condition, orderBy);
			return (Multa[]) list.toArray(new Multa[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa[] listMultaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryMulta(session, condition, orderBy, lockMode);
			return (Multa[]) list.toArray(new Multa[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa loadMultaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadMultaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa loadMultaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadMultaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa loadMultaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Multa[] multas = listMultaByQuery(session, condition, orderBy);
		if (multas != null && multas.length > 0)
			return multas[0];
		else
			return null;
	}
	
	public static Multa loadMultaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Multa[] multas = listMultaByQuery(session, condition, orderBy, lockMode);
		if (multas != null && multas.length > 0)
			return multas[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateMultaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateMultaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateMultaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateMultaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateMultaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Multa as Multa");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateMultaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Multa as Multa");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Multa", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa createMulta() {
		return new orm.Multa();
	}
	
	public static boolean save(orm.Multa multa) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().saveObject(multa);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Multa multa) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().deleteObject(multa);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Multa multa)throws PersistentException {
		try {
			if(multa.getCarabinero() != null) {
				multa.getCarabinero().multa.remove(multa);
			}
			
			orm.Vehiculos[] lVehiculosid_vehiculos = multa.vehiculosid_vehiculo.toArray();
			for(int i = 0; i < lVehiculosid_vehiculos.length; i++) {
				lVehiculosid_vehiculos[i].multa.remove(multa);
			}
			orm.Solicitud_cambio_fecha[] lSolicitud_cambio_fechas = multa.solicitud_cambio_fecha.toArray();
			for(int i = 0; i < lSolicitud_cambio_fechas.length; i++) {
				lSolicitud_cambio_fechas[i].setMulta(null);
			}
			if(multa.getMultavehiculo() != null) {
				multa.getMultavehiculo().setMulta(null);
			}
			
			orm.Infractor_persona_multa[] lInfractor_persona_multas = multa.infractor_persona_multa.toArray();
			for(int i = 0; i < lInfractor_persona_multas.length; i++) {
				lInfractor_persona_multas[i].setMulta(null);
			}
			if(multa.getPago() != null) {
				multa.getPago().setMulta(null);
			}
			
			if(multa.getResolucion() != null) {
				multa.getResolucion().setMulta(null);
			}
			
			if(multa.getSolicitud_exorto() != null) {
				multa.getSolicitud_exorto().setMulta(null);
			}
			
			return delete(multa);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Multa multa, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(multa.getCarabinero() != null) {
				multa.getCarabinero().multa.remove(multa);
			}
			
			orm.Vehiculos[] lVehiculosid_vehiculos = multa.vehiculosid_vehiculo.toArray();
			for(int i = 0; i < lVehiculosid_vehiculos.length; i++) {
				lVehiculosid_vehiculos[i].multa.remove(multa);
			}
			orm.Solicitud_cambio_fecha[] lSolicitud_cambio_fechas = multa.solicitud_cambio_fecha.toArray();
			for(int i = 0; i < lSolicitud_cambio_fechas.length; i++) {
				lSolicitud_cambio_fechas[i].setMulta(null);
			}
			if(multa.getMultavehiculo() != null) {
				multa.getMultavehiculo().setMulta(null);
			}
			
			orm.Infractor_persona_multa[] lInfractor_persona_multas = multa.infractor_persona_multa.toArray();
			for(int i = 0; i < lInfractor_persona_multas.length; i++) {
				lInfractor_persona_multas[i].setMulta(null);
			}
			if(multa.getPago() != null) {
				multa.getPago().setMulta(null);
			}
			
			if(multa.getResolucion() != null) {
				multa.getResolucion().setMulta(null);
			}
			
			if(multa.getSolicitud_exorto() != null) {
				multa.getSolicitud_exorto().setMulta(null);
			}
			
			try {
				session.delete(multa);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Multa multa) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().refresh(multa);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Multa multa) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().evict(multa);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Multa loadMultaByCriteria(MultaCriteria multaCriteria) {
		Multa[] multas = listMultaByCriteria(multaCriteria);
		if(multas == null || multas.length == 0) {
			return null;
		}
		return multas[0];
	}
	
	public static Multa[] listMultaByCriteria(MultaCriteria multaCriteria) {
		return multaCriteria.listMulta();
	}
}
