/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Infractor_persona_multaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	
	public Infractor_persona_multaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
	}
	
	public Infractor_persona_multaCriteria(PersistentSession session) {
		this(session.createCriteria(Infractor_persona_multa.class));
	}
	
	public Infractor_persona_multaCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public Infractor_personaCriteria createInfractor_personaCriteria() {
		return new Infractor_personaCriteria(createCriteria("infractor_persona"));
	}
	
	public MultaCriteria createMultaCriteria() {
		return new MultaCriteria(createCriteria("multa"));
	}
	
	public Infractor_persona_multa uniqueInfractor_persona_multa() {
		return (Infractor_persona_multa) super.uniqueResult();
	}
	
	public Infractor_persona_multa[] listInfractor_persona_multa() {
		java.util.List list = super.list();
		return (Infractor_persona_multa[]) list.toArray(new Infractor_persona_multa[list.size()]);
	}
}

