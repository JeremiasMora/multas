/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TesoreraCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	
	public TesoreraCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
	}
	
	public TesoreraCriteria(PersistentSession session) {
		this(session.createCriteria(Tesorera.class));
	}
	
	public TesoreraCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public PersonaCriteria createPersonaid_usuarioCriteria() {
		return new PersonaCriteria(createCriteria("personaid_usuario"));
	}
	
	public PagoCriteria createPagoCriteria() {
		return new PagoCriteria(createCriteria("ORM_pago"));
	}
	
	public Tesorera uniqueTesorera() {
		return (Tesorera) super.uniqueResult();
	}
	
	public Tesorera[] listTesorera() {
		java.util.List list = super.list();
		return (Tesorera[]) list.toArray(new Tesorera[list.size()]);
	}
}

