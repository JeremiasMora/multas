/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CarabineroDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	
	public CarabineroDetachedCriteria() {
		super(orm.Carabinero.class, orm.CarabineroCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public CarabineroDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.CarabineroCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria createPersonaid_usuarioCriteria() {
		return new PersonaDetachedCriteria(createCriteria("personaid_usuario"));
	}
	
	public MultaDetachedCriteria createMultaCriteria() {
		return new MultaDetachedCriteria(createCriteria("ORM_multa"));
	}
	
	public Carabinero uniqueCarabinero(PersistentSession session) {
		return (Carabinero) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Carabinero[] listCarabinero(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Carabinero[]) list.toArray(new Carabinero[list.size()]);
	}
}

