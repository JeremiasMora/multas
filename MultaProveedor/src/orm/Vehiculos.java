/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
/**
 * Esta tabla debería almacenar todas las patentes con su información del país, tenga o no una persona
 */
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="vehiculos")
public class Vehiculos implements Serializable {
	public Vehiculos() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_VEHICULOS_MULTA) {
			return ORM_multa;
		}
		else if (key == orm.ORMConstants.KEY_VEHICULOS_MULTAVEHICULO) {
			return ORM_multavehiculo;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_VEHICULOS_PERSONAID_USUARIO) {
			this.personaid_usuario = (orm.Persona) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="id_vehiculo", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_VEHICULOS_ID_VEHICULO_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_VEHICULOS_ID_VEHICULO_GENERATOR", strategy="increment")	
	private int id_vehiculo;
	
	@Column(name="marca", nullable=true, length=255)	
	private String marca;
	
	@Column(name="modelo", nullable=true, length=255)	
	private String modelo;
	
	@Column(name="anio", nullable=true, length=10)	
	private Integer anio;
	
	@ManyToOne(targetEntity=orm.Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaid_usuario", referencedColumnName="id_usuario", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Persona personaid_usuario;
	
	@Column(name="patente", nullable=false, unique=true, length=255)	
	private String patente;
	
	@ManyToMany(mappedBy="ORM_vehiculosid_vehiculo", targetEntity=orm.Multa.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_multa = new java.util.HashSet();
	
	@OneToMany(mappedBy="vehiculosid_vehiculo", targetEntity=orm.Multavehiculo.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_multavehiculo = new java.util.HashSet();
	
	private void setId_vehiculo(int value) {
		this.id_vehiculo = value;
	}
	
	public int getId_vehiculo() {
		return id_vehiculo;
	}
	
	public int getORMID() {
		return getId_vehiculo();
	}
	
	public void setMarca(String value) {
		this.marca = value;
	}
	
	public String getMarca() {
		return marca;
	}
	
	public void setModelo(String value) {
		this.modelo = value;
	}
	
	public String getModelo() {
		return modelo;
	}
	
	public void setAnio(int value) {
		setAnio(new Integer(value));
	}
	
	public void setAnio(Integer value) {
		this.anio = value;
	}
	
	public Integer getAnio() {
		return anio;
	}
	
	/**
	 * Este atributo es único, ya que no existen 2 vehículos con una misma patente, y a la vez es campo obligatorio, ya que será usado para busqueda.
	 */
	public void setPatente(String value) {
		this.patente = value;
	}
	
	/**
	 * Este atributo es único, ya que no existen 2 vehículos con una misma patente, y a la vez es campo obligatorio, ya que será usado para busqueda.
	 */
	public String getPatente() {
		return patente;
	}
	
	public void setPersonaid_usuario(orm.Persona value) {
		if (personaid_usuario != null) {
			personaid_usuario.vehiculos.remove(this);
		}
		if (value != null) {
			value.vehiculos.add(this);
		}
	}
	
	public orm.Persona getPersonaid_usuario() {
		return personaid_usuario;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Personaid_usuario(orm.Persona value) {
		this.personaid_usuario = value;
	}
	
	private orm.Persona getORM_Personaid_usuario() {
		return personaid_usuario;
	}
	
	private void setORM_Multa(java.util.Set value) {
		this.ORM_multa = value;
	}
	
	private java.util.Set getORM_Multa() {
		return ORM_multa;
	}
	
	@Transient	
	public final orm.MultaSetCollection multa = new orm.MultaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_VEHICULOS_MULTA, orm.ORMConstants.KEY_MULTA_VEHICULOSID_VEHICULO, orm.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	private void setORM_Multavehiculo(java.util.Set value) {
		this.ORM_multavehiculo = value;
	}
	
	private java.util.Set getORM_Multavehiculo() {
		return ORM_multavehiculo;
	}
	
	@Transient	
	public final orm.MultavehiculoSetCollection multavehiculo = new orm.MultavehiculoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_VEHICULOS_MULTAVEHICULO, orm.ORMConstants.KEY_MULTAVEHICULO_VEHICULOSID_VEHICULO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId_vehiculo());
	}
	
}
