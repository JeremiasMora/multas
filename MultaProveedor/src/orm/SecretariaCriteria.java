/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class SecretariaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	
	public SecretariaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
	}
	
	public SecretariaCriteria(PersistentSession session) {
		this(session.createCriteria(Secretaria.class));
	}
	
	public SecretariaCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public PersonaCriteria createPersonaid_usuarioCriteria() {
		return new PersonaCriteria(createCriteria("personaid_usuario"));
	}
	
	public Secretaria uniqueSecretaria() {
		return (Secretaria) super.uniqueResult();
	}
	
	public Secretaria[] listSecretaria() {
		java.util.List list = super.list();
		return (Secretaria[]) list.toArray(new Secretaria[list.size()]);
	}
}

