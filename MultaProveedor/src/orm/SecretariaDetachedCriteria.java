/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class SecretariaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	
	public SecretariaDetachedCriteria() {
		super(orm.Secretaria.class, orm.SecretariaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public SecretariaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.SecretariaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria createPersonaid_usuarioCriteria() {
		return new PersonaDetachedCriteria(createCriteria("personaid_usuario"));
	}
	
	public Secretaria uniqueSecretaria(PersistentSession session) {
		return (Secretaria) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Secretaria[] listSecretaria(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Secretaria[]) list.toArray(new Secretaria[list.size()]);
	}
}

