/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class TesoreraDAO {
	public static Tesorera loadTesoreraByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadTesoreraByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera getTesoreraByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getTesoreraByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera loadTesoreraByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadTesoreraByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera getTesoreraByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getTesoreraByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera loadTesoreraByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Tesorera) session.load(orm.Tesorera.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera getTesoreraByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Tesorera) session.get(orm.Tesorera.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera loadTesoreraByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tesorera) session.load(orm.Tesorera.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera getTesoreraByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Tesorera) session.get(orm.Tesorera.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTesorera(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return queryTesorera(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTesorera(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return queryTesorera(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera[] listTesoreraByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listTesoreraByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera[] listTesoreraByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listTesoreraByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTesorera(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Tesorera as Tesorera");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTesorera(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Tesorera as Tesorera");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tesorera", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera[] listTesoreraByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryTesorera(session, condition, orderBy);
			return (Tesorera[]) list.toArray(new Tesorera[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera[] listTesoreraByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryTesorera(session, condition, orderBy, lockMode);
			return (Tesorera[]) list.toArray(new Tesorera[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera loadTesoreraByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadTesoreraByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera loadTesoreraByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadTesoreraByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera loadTesoreraByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Tesorera[] tesoreras = listTesoreraByQuery(session, condition, orderBy);
		if (tesoreras != null && tesoreras.length > 0)
			return tesoreras[0];
		else
			return null;
	}
	
	public static Tesorera loadTesoreraByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Tesorera[] tesoreras = listTesoreraByQuery(session, condition, orderBy, lockMode);
		if (tesoreras != null && tesoreras.length > 0)
			return tesoreras[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateTesoreraByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateTesoreraByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTesoreraByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateTesoreraByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTesoreraByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Tesorera as Tesorera");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTesoreraByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Tesorera as Tesorera");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Tesorera", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera createTesorera() {
		return new orm.Tesorera();
	}
	
	public static boolean save(orm.Tesorera tesorera) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().saveObject(tesorera);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Tesorera tesorera) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().deleteObject(tesorera);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Tesorera tesorera)throws PersistentException {
		try {
			if(tesorera.getPersonaid_usuario() != null) {
				tesorera.getPersonaid_usuario().setTesorera(null);
			}
			
			orm.Pago[] lPagos = tesorera.pago.toArray();
			for(int i = 0; i < lPagos.length; i++) {
				lPagos[i].setTesorera(null);
			}
			return delete(tesorera);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Tesorera tesorera, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(tesorera.getPersonaid_usuario() != null) {
				tesorera.getPersonaid_usuario().setTesorera(null);
			}
			
			orm.Pago[] lPagos = tesorera.pago.toArray();
			for(int i = 0; i < lPagos.length; i++) {
				lPagos[i].setTesorera(null);
			}
			try {
				session.delete(tesorera);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Tesorera tesorera) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().refresh(tesorera);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Tesorera tesorera) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().evict(tesorera);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Tesorera loadTesoreraByCriteria(TesoreraCriteria tesoreraCriteria) {
		Tesorera[] tesoreras = listTesoreraByCriteria(tesoreraCriteria);
		if(tesoreras == null || tesoreras.length == 0) {
			return null;
		}
		return tesoreras[0];
	}
	
	public static Tesorera[] listTesoreraByCriteria(TesoreraCriteria tesoreraCriteria) {
		return tesoreraCriteria.listTesorera();
	}
}
