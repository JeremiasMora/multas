/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ResolucionDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final IntegerExpression valorpagar;
	
	public ResolucionDetachedCriteria() {
		super(orm.Resolucion.class, orm.ResolucionCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		valorpagar = new IntegerExpression("valorpagar", this.getDetachedCriteria());
	}
	
	public ResolucionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.ResolucionCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		valorpagar = new IntegerExpression("valorpagar", this.getDetachedCriteria());
	}
	
	public JuzgadoDetachedCriteria createJuzgadoCriteria() {
		return new JuzgadoDetachedCriteria(createCriteria("juzgado"));
	}
	
	public MultaDetachedCriteria createMultaCriteria() {
		return new MultaDetachedCriteria(createCriteria("multa"));
	}
	
	public Resolucion uniqueResolucion(PersistentSession session) {
		return (Resolucion) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Resolucion[] listResolucion(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Resolucion[]) list.toArray(new Resolucion[list.size()]);
	}
}

