/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Infractor_persona_multaDAO {
	public static Infractor_persona_multa loadInfractor_persona_multaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadInfractor_persona_multaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa getInfractor_persona_multaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getInfractor_persona_multaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa loadInfractor_persona_multaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadInfractor_persona_multaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa getInfractor_persona_multaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getInfractor_persona_multaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa loadInfractor_persona_multaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Infractor_persona_multa) session.load(orm.Infractor_persona_multa.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa getInfractor_persona_multaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Infractor_persona_multa) session.get(orm.Infractor_persona_multa.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa loadInfractor_persona_multaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Infractor_persona_multa) session.load(orm.Infractor_persona_multa.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa getInfractor_persona_multaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Infractor_persona_multa) session.get(orm.Infractor_persona_multa.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryInfractor_persona_multa(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return queryInfractor_persona_multa(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryInfractor_persona_multa(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return queryInfractor_persona_multa(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa[] listInfractor_persona_multaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listInfractor_persona_multaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa[] listInfractor_persona_multaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listInfractor_persona_multaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryInfractor_persona_multa(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Infractor_persona_multa as Infractor_persona_multa");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryInfractor_persona_multa(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Infractor_persona_multa as Infractor_persona_multa");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Infractor_persona_multa", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa[] listInfractor_persona_multaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryInfractor_persona_multa(session, condition, orderBy);
			return (Infractor_persona_multa[]) list.toArray(new Infractor_persona_multa[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa[] listInfractor_persona_multaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryInfractor_persona_multa(session, condition, orderBy, lockMode);
			return (Infractor_persona_multa[]) list.toArray(new Infractor_persona_multa[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa loadInfractor_persona_multaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadInfractor_persona_multaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa loadInfractor_persona_multaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadInfractor_persona_multaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa loadInfractor_persona_multaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Infractor_persona_multa[] infractor_persona_multas = listInfractor_persona_multaByQuery(session, condition, orderBy);
		if (infractor_persona_multas != null && infractor_persona_multas.length > 0)
			return infractor_persona_multas[0];
		else
			return null;
	}
	
	public static Infractor_persona_multa loadInfractor_persona_multaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Infractor_persona_multa[] infractor_persona_multas = listInfractor_persona_multaByQuery(session, condition, orderBy, lockMode);
		if (infractor_persona_multas != null && infractor_persona_multas.length > 0)
			return infractor_persona_multas[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateInfractor_persona_multaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateInfractor_persona_multaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateInfractor_persona_multaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateInfractor_persona_multaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateInfractor_persona_multaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Infractor_persona_multa as Infractor_persona_multa");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateInfractor_persona_multaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Infractor_persona_multa as Infractor_persona_multa");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Infractor_persona_multa", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa createInfractor_persona_multa() {
		return new orm.Infractor_persona_multa();
	}
	
	public static boolean save(orm.Infractor_persona_multa infractor_persona_multa) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().saveObject(infractor_persona_multa);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Infractor_persona_multa infractor_persona_multa) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().deleteObject(infractor_persona_multa);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Infractor_persona_multa infractor_persona_multa)throws PersistentException {
		try {
			if(infractor_persona_multa.getInfractor_persona() != null) {
				infractor_persona_multa.getInfractor_persona().infractor_persona_multa.remove(infractor_persona_multa);
			}
			
			if(infractor_persona_multa.getMulta() != null) {
				infractor_persona_multa.getMulta().infractor_persona_multa.remove(infractor_persona_multa);
			}
			
			return delete(infractor_persona_multa);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Infractor_persona_multa infractor_persona_multa, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(infractor_persona_multa.getInfractor_persona() != null) {
				infractor_persona_multa.getInfractor_persona().infractor_persona_multa.remove(infractor_persona_multa);
			}
			
			if(infractor_persona_multa.getMulta() != null) {
				infractor_persona_multa.getMulta().infractor_persona_multa.remove(infractor_persona_multa);
			}
			
			try {
				session.delete(infractor_persona_multa);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Infractor_persona_multa infractor_persona_multa) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().refresh(infractor_persona_multa);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Infractor_persona_multa infractor_persona_multa) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().evict(infractor_persona_multa);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Infractor_persona_multa loadInfractor_persona_multaByCriteria(Infractor_persona_multaCriteria infractor_persona_multaCriteria) {
		Infractor_persona_multa[] infractor_persona_multas = listInfractor_persona_multaByCriteria(infractor_persona_multaCriteria);
		if(infractor_persona_multas == null || infractor_persona_multas.length == 0) {
			return null;
		}
		return infractor_persona_multas[0];
	}
	
	public static Infractor_persona_multa[] listInfractor_persona_multaByCriteria(Infractor_persona_multaCriteria infractor_persona_multaCriteria) {
		return infractor_persona_multaCriteria.listInfractor_persona_multa();
	}
}
