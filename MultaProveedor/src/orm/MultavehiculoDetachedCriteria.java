/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MultavehiculoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	
	public MultavehiculoDetachedCriteria() {
		super(orm.Multavehiculo.class, orm.MultavehiculoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public MultavehiculoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.MultavehiculoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public VehiculosDetachedCriteria createVehiculosid_vehiculoCriteria() {
		return new VehiculosDetachedCriteria(createCriteria("vehiculosid_vehiculo"));
	}
	
	public MultaDetachedCriteria createMultaCriteria() {
		return new MultaDetachedCriteria(createCriteria("multa"));
	}
	
	public Multavehiculo uniqueMultavehiculo(PersistentSession session) {
		return (Multavehiculo) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Multavehiculo[] listMultavehiculo(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Multavehiculo[]) list.toArray(new Multavehiculo[list.size()]);
	}
}

