/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="tesorera")
public class Tesorera implements Serializable {
	public Tesorera() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_TESORERA_PAGO) {
			return ORM_pago;
		}
		
		return null;
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_TESORERA_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_TESORERA_ID_GENERATOR", strategy="increment")	
	private int id;
	
	@OneToOne(targetEntity=orm.Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaid_usuario", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Persona personaid_usuario;
	
	@OneToMany(mappedBy="tesorera", targetEntity=orm.Pago.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_pago = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setPersonaid_usuario(orm.Persona value) {
		if (this.personaid_usuario != value) {
			orm.Persona lpersonaid_usuario = this.personaid_usuario;
			this.personaid_usuario = value;
			if (value != null) {
				personaid_usuario.setTesorera(this);
			}
			else {
				lpersonaid_usuario.setTesorera(null);
			}
		}
	}
	
	public orm.Persona getPersonaid_usuario() {
		return personaid_usuario;
	}
	
	private void setORM_Pago(java.util.Set value) {
		this.ORM_pago = value;
	}
	
	private java.util.Set getORM_Pago() {
		return ORM_pago;
	}
	
	@Transient	
	public final orm.PagoSetCollection pago = new orm.PagoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_TESORERA_PAGO, orm.ORMConstants.KEY_PAGO_TESORERA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
