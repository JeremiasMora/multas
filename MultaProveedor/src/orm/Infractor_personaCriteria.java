/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Infractor_personaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	
	public Infractor_personaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
	}
	
	public Infractor_personaCriteria(PersistentSession session) {
		this(session.createCriteria(Infractor_persona.class));
	}
	
	public Infractor_personaCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public PersonaCriteria createPersonaid_usuarioCriteria() {
		return new PersonaCriteria(createCriteria("personaid_usuario"));
	}
	
	public Infractor_persona_multaCriteria createInfractor_persona_multaCriteria() {
		return new Infractor_persona_multaCriteria(createCriteria("ORM_infractor_persona_multa"));
	}
	
	public Infractor_persona uniqueInfractor_persona() {
		return (Infractor_persona) super.uniqueResult();
	}
	
	public Infractor_persona[] listInfractor_persona() {
		java.util.List list = super.list();
		return (Infractor_persona[]) list.toArray(new Infractor_persona[list.size()]);
	}
}

