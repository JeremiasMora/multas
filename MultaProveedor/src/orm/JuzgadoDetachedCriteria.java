/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class JuzgadoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	
	public JuzgadoDetachedCriteria() {
		super(orm.Juzgado.class, orm.JuzgadoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public JuzgadoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.JuzgadoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria createPersonaid_usuarioCriteria() {
		return new PersonaDetachedCriteria(createCriteria("personaid_usuario"));
	}
	
	public ResolucionDetachedCriteria createResolucionCriteria() {
		return new ResolucionDetachedCriteria(createCriteria("ORM_resolucion"));
	}
	
	public Juzgado uniqueJuzgado(PersistentSession session) {
		return (Juzgado) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Juzgado[] listJuzgado(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Juzgado[]) list.toArray(new Juzgado[list.size()]);
	}
}

