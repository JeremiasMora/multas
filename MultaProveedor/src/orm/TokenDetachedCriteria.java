/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TokenDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression token;
	public final StringExpression fecha_inicio;
	public final StringExpression fecha_fin;
	
	public TokenDetachedCriteria() {
		super(orm.Token.class, orm.TokenCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		token = new StringExpression("token", this.getDetachedCriteria());
		fecha_inicio = new StringExpression("fecha_inicio", this.getDetachedCriteria());
		fecha_fin = new StringExpression("fecha_fin", this.getDetachedCriteria());
	}
	
	public TokenDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.TokenCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		token = new StringExpression("token", this.getDetachedCriteria());
		fecha_inicio = new StringExpression("fecha_inicio", this.getDetachedCriteria());
		fecha_fin = new StringExpression("fecha_fin", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria createPersonaid_usuarioCriteria() {
		return new PersonaDetachedCriteria(createCriteria("personaid_usuario"));
	}
	
	public Token uniqueToken(PersistentSession session) {
		return (Token) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Token[] listToken(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Token[]) list.toArray(new Token[list.size()]);
	}
}

