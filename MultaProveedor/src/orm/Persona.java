/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
/**
 * Tabla contenedora de toda la información de los actores del sistema.
 * 
 * Para el uso del login, se usaran los campos rut y pass (contraseña).
 */
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="persona")
public class Persona implements Serializable {
	public Persona() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_PERSONA_JUZGADO) {
			return ORM_juzgado;
		}
		else if (key == orm.ORMConstants.KEY_PERSONA_VEHICULOS) {
			return ORM_vehiculos;
		}
		
		return null;
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	@Column(name="id_usuario", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_PERSONA_ID_USUARIO_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_PERSONA_ID_USUARIO_GENERATOR", strategy="increment")	
	private int id_usuario;
	
	@Column(name="rut", nullable=false, unique=true, length=12)	
	private String rut;
	
	@Column(name="pass", nullable=false, length=255)	
	private String pass;
	
	@Column(name="nombre", nullable=true, length=255)	
	private String nombre;
	
	@Column(name="apellido", nullable=true, length=255)	
	private String apellido;
	
	@Column(name="direccion", nullable=true, length=255)	
	private String direccion;
	
	@OneToMany(mappedBy="personaid_usuario", targetEntity=orm.Juzgado.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_juzgado = new java.util.HashSet();
	
	@OneToMany(mappedBy="personaid_usuario", targetEntity=orm.Vehiculos.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_vehiculos = new java.util.HashSet();
	
	@OneToOne(mappedBy="personaid_usuario", targetEntity=orm.Secretaria.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Secretaria secretaria;
	
	@OneToOne(mappedBy="personaid_usuario", targetEntity=orm.Carabinero.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Carabinero carabinero;
	
	@OneToOne(mappedBy="personaid_usuario", targetEntity=orm.Token.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Token token;
	
	@OneToOne(mappedBy="personaid_usuario", targetEntity=orm.Infractor_persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Infractor_persona infractor_persona;
	
	@OneToOne(mappedBy="personaid_usuario", targetEntity=orm.Tesorera.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Tesorera tesorera;
	
	private void setId_usuario(int value) {
		this.id_usuario = value;
	}
	
	public int getId_usuario() {
		return id_usuario;
	}
	
	public int getORMID() {
		return getId_usuario();
	}
	
	/**
	 * El rut debe ser unico y obligatorio, ya que es usado para logearse
	 */
	public void setRut(String value) {
		this.rut = value;
	}
	
	/**
	 * El rut debe ser unico y obligatorio, ya que es usado para logearse
	 */
	public String getRut() {
		return rut;
	}
	
	/**
	 * Contraseña usada para logearse, por ende es un campo obligatorio.
	 */
	public void setPass(String value) {
		this.pass = value;
	}
	
	/**
	 * Contraseña usada para logearse, por ende es un campo obligatorio.
	 */
	public String getPass() {
		return pass;
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setApellido(String value) {
		this.apellido = value;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setDireccion(String value) {
		this.direccion = value;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	private void setORM_Juzgado(java.util.Set value) {
		this.ORM_juzgado = value;
	}
	
	private java.util.Set getORM_Juzgado() {
		return ORM_juzgado;
	}
	
	@Transient	
	public final orm.JuzgadoSetCollection juzgado = new orm.JuzgadoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_JUZGADO, orm.ORMConstants.KEY_JUZGADO_PERSONAID_USUARIO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Vehiculos(java.util.Set value) {
		this.ORM_vehiculos = value;
	}
	
	private java.util.Set getORM_Vehiculos() {
		return ORM_vehiculos;
	}
	
	@Transient	
	public final orm.VehiculosSetCollection vehiculos = new orm.VehiculosSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_VEHICULOS, orm.ORMConstants.KEY_VEHICULOS_PERSONAID_USUARIO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public void setSecretaria(orm.Secretaria value) {
		if (this.secretaria != value) {
			orm.Secretaria lsecretaria = this.secretaria;
			this.secretaria = value;
			if (value != null) {
				secretaria.setPersonaid_usuario(this);
			}
			else {
				lsecretaria.setPersonaid_usuario(null);
			}
		}
	}
	
	public orm.Secretaria getSecretaria() {
		return secretaria;
	}
	
	public void setCarabinero(orm.Carabinero value) {
		if (this.carabinero != value) {
			orm.Carabinero lcarabinero = this.carabinero;
			this.carabinero = value;
			if (value != null) {
				carabinero.setPersonaid_usuario(this);
			}
			else {
				lcarabinero.setPersonaid_usuario(null);
			}
		}
	}
	
	public orm.Carabinero getCarabinero() {
		return carabinero;
	}
	
	public void setToken(orm.Token value) {
		if (this.token != value) {
			orm.Token ltoken = this.token;
			this.token = value;
			if (value != null) {
				token.setPersonaid_usuario(this);
			}
			else {
				ltoken.setPersonaid_usuario(null);
			}
		}
	}
	
	public orm.Token getToken() {
		return token;
	}
	
	public void setInfractor_persona(orm.Infractor_persona value) {
		if (this.infractor_persona != value) {
			orm.Infractor_persona linfractor_persona = this.infractor_persona;
			this.infractor_persona = value;
			if (value != null) {
				infractor_persona.setPersonaid_usuario(this);
			}
			else {
				linfractor_persona.setPersonaid_usuario(null);
			}
		}
	}
	
	public orm.Infractor_persona getInfractor_persona() {
		return infractor_persona;
	}
	
	public void setTesorera(orm.Tesorera value) {
		if (this.tesorera != value) {
			orm.Tesorera ltesorera = this.tesorera;
			this.tesorera = value;
			if (value != null) {
				tesorera.setPersonaid_usuario(this);
			}
			else {
				ltesorera.setPersonaid_usuario(null);
			}
		}
	}
	
	public orm.Tesorera getTesorera() {
		return tesorera;
	}
	
	public String toString() {
		return String.valueOf(getId_usuario());
	}
	
}
