/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Infractor_personaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	
	public Infractor_personaDetachedCriteria() {
		super(orm.Infractor_persona.class, orm.Infractor_personaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public Infractor_personaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Infractor_personaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria createPersonaid_usuarioCriteria() {
		return new PersonaDetachedCriteria(createCriteria("personaid_usuario"));
	}
	
	public Infractor_persona_multaDetachedCriteria createInfractor_persona_multaCriteria() {
		return new Infractor_persona_multaDetachedCriteria(createCriteria("ORM_infractor_persona_multa"));
	}
	
	public Infractor_persona uniqueInfractor_persona(PersistentSession session) {
		return (Infractor_persona) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Infractor_persona[] listInfractor_persona(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Infractor_persona[]) list.toArray(new Infractor_persona[list.size()]);
	}
}

