/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class VehiculosDAO {
	public static Vehiculos loadVehiculosByORMID(int id_vehiculo) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadVehiculosByORMID(session, id_vehiculo);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos getVehiculosByORMID(int id_vehiculo) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getVehiculosByORMID(session, id_vehiculo);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos loadVehiculosByORMID(int id_vehiculo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadVehiculosByORMID(session, id_vehiculo, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos getVehiculosByORMID(int id_vehiculo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getVehiculosByORMID(session, id_vehiculo, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos loadVehiculosByORMID(PersistentSession session, int id_vehiculo) throws PersistentException {
		try {
			return (Vehiculos) session.load(orm.Vehiculos.class, new Integer(id_vehiculo));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos getVehiculosByORMID(PersistentSession session, int id_vehiculo) throws PersistentException {
		try {
			return (Vehiculos) session.get(orm.Vehiculos.class, new Integer(id_vehiculo));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos loadVehiculosByORMID(PersistentSession session, int id_vehiculo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Vehiculos) session.load(orm.Vehiculos.class, new Integer(id_vehiculo), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos getVehiculosByORMID(PersistentSession session, int id_vehiculo, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Vehiculos) session.get(orm.Vehiculos.class, new Integer(id_vehiculo), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryVehiculos(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return queryVehiculos(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryVehiculos(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return queryVehiculos(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos[] listVehiculosByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listVehiculosByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos[] listVehiculosByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listVehiculosByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryVehiculos(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Vehiculos as Vehiculos");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryVehiculos(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Vehiculos as Vehiculos");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Vehiculos", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos[] listVehiculosByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryVehiculos(session, condition, orderBy);
			return (Vehiculos[]) list.toArray(new Vehiculos[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos[] listVehiculosByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryVehiculos(session, condition, orderBy, lockMode);
			return (Vehiculos[]) list.toArray(new Vehiculos[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos loadVehiculosByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadVehiculosByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos loadVehiculosByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadVehiculosByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos loadVehiculosByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Vehiculos[] vehiculoses = listVehiculosByQuery(session, condition, orderBy);
		if (vehiculoses != null && vehiculoses.length > 0)
			return vehiculoses[0];
		else
			return null;
	}
	
	public static Vehiculos loadVehiculosByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Vehiculos[] vehiculoses = listVehiculosByQuery(session, condition, orderBy, lockMode);
		if (vehiculoses != null && vehiculoses.length > 0)
			return vehiculoses[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateVehiculosByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateVehiculosByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateVehiculosByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateVehiculosByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateVehiculosByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Vehiculos as Vehiculos");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateVehiculosByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Vehiculos as Vehiculos");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Vehiculos", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos createVehiculos() {
		return new orm.Vehiculos();
	}
	
	public static boolean save(orm.Vehiculos vehiculos) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().saveObject(vehiculos);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Vehiculos vehiculos) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().deleteObject(vehiculos);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Vehiculos vehiculos)throws PersistentException {
		try {
			if(vehiculos.getPersonaid_usuario() != null) {
				vehiculos.getPersonaid_usuario().vehiculos.remove(vehiculos);
			}
			
			orm.Multa[] lMultas = vehiculos.multa.toArray();
			for(int i = 0; i < lMultas.length; i++) {
				lMultas[i].vehiculosid_vehiculo.remove(vehiculos);
			}
			orm.Multavehiculo[] lMultavehiculos = vehiculos.multavehiculo.toArray();
			for(int i = 0; i < lMultavehiculos.length; i++) {
				lMultavehiculos[i].setVehiculosid_vehiculo(null);
			}
			return delete(vehiculos);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Vehiculos vehiculos, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(vehiculos.getPersonaid_usuario() != null) {
				vehiculos.getPersonaid_usuario().vehiculos.remove(vehiculos);
			}
			
			orm.Multa[] lMultas = vehiculos.multa.toArray();
			for(int i = 0; i < lMultas.length; i++) {
				lMultas[i].vehiculosid_vehiculo.remove(vehiculos);
			}
			orm.Multavehiculo[] lMultavehiculos = vehiculos.multavehiculo.toArray();
			for(int i = 0; i < lMultavehiculos.length; i++) {
				lMultavehiculos[i].setVehiculosid_vehiculo(null);
			}
			try {
				session.delete(vehiculos);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Vehiculos vehiculos) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().refresh(vehiculos);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Vehiculos vehiculos) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().evict(vehiculos);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Vehiculos loadVehiculosByCriteria(VehiculosCriteria vehiculosCriteria) {
		Vehiculos[] vehiculoses = listVehiculosByCriteria(vehiculosCriteria);
		if(vehiculoses == null || vehiculoses.length == 0) {
			return null;
		}
		return vehiculoses[0];
	}
	
	public static Vehiculos[] listVehiculosByCriteria(VehiculosCriteria vehiculosCriteria) {
		return vehiculosCriteria.listVehiculos();
	}
}
