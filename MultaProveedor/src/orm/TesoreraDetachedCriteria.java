/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TesoreraDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	
	public TesoreraDetachedCriteria() {
		super(orm.Tesorera.class, orm.TesoreraCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public TesoreraDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.TesoreraCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria createPersonaid_usuarioCriteria() {
		return new PersonaDetachedCriteria(createCriteria("personaid_usuario"));
	}
	
	public PagoDetachedCriteria createPagoCriteria() {
		return new PagoDetachedCriteria(createCriteria("ORM_pago"));
	}
	
	public Tesorera uniqueTesorera(PersistentSession session) {
		return (Tesorera) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Tesorera[] listTesorera(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Tesorera[]) list.toArray(new Tesorera[list.size()]);
	}
}

