/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="secretaria")
public class Secretaria implements Serializable {
	public Secretaria() {
	}
	
	@Column(name="id", nullable=false, unique=true)	
	@Id	
	@GeneratedValue(generator="ORM_SECRETARIA_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_SECRETARIA_ID_GENERATOR", strategy="identity")	
	private int id;
	
	@OneToOne(targetEntity=orm.Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaid_usuario", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Persona personaid_usuario;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setPersonaid_usuario(orm.Persona value) {
		if (this.personaid_usuario != value) {
			orm.Persona lpersonaid_usuario = this.personaid_usuario;
			this.personaid_usuario = value;
			if (value != null) {
				personaid_usuario.setSecretaria(this);
			}
			else {
				lpersonaid_usuario.setSecretaria(null);
			}
		}
	}
	
	public orm.Persona getPersonaid_usuario() {
		return personaid_usuario;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
