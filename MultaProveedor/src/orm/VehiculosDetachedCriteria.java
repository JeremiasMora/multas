/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class VehiculosDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id_vehiculo;
	public final StringExpression marca;
	public final StringExpression modelo;
	public final IntegerExpression anio;
	public final StringExpression patente;
	
	public VehiculosDetachedCriteria() {
		super(orm.Vehiculos.class, orm.VehiculosCriteria.class);
		id_vehiculo = new IntegerExpression("id_vehiculo", this.getDetachedCriteria());
		marca = new StringExpression("marca", this.getDetachedCriteria());
		modelo = new StringExpression("modelo", this.getDetachedCriteria());
		anio = new IntegerExpression("anio", this.getDetachedCriteria());
		patente = new StringExpression("patente", this.getDetachedCriteria());
	}
	
	public VehiculosDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.VehiculosCriteria.class);
		id_vehiculo = new IntegerExpression("id_vehiculo", this.getDetachedCriteria());
		marca = new StringExpression("marca", this.getDetachedCriteria());
		modelo = new StringExpression("modelo", this.getDetachedCriteria());
		anio = new IntegerExpression("anio", this.getDetachedCriteria());
		patente = new StringExpression("patente", this.getDetachedCriteria());
	}
	
	public PersonaDetachedCriteria createPersonaid_usuarioCriteria() {
		return new PersonaDetachedCriteria(createCriteria("personaid_usuario"));
	}
	
	public MultaDetachedCriteria createMultaCriteria() {
		return new MultaDetachedCriteria(createCriteria("ORM_multa"));
	}
	
	public MultavehiculoDetachedCriteria createMultavehiculoCriteria() {
		return new MultavehiculoDetachedCriteria(createCriteria("ORM_multavehiculo"));
	}
	
	public Vehiculos uniqueVehiculos(PersistentSession session) {
		return (Vehiculos) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Vehiculos[] listVehiculos(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Vehiculos[]) list.toArray(new Vehiculos[list.size()]);
	}
}

