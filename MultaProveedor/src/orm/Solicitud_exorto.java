/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
/**
 * Esta tabla almacena solicitudes de exorto de una multa
 */
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="solicitud_exorto")
public class Solicitud_exorto implements Serializable {
	public Solicitud_exorto() {
	}
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_SOLICITUD_EXORTO_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_SOLICITUD_EXORTO_ID_GENERATOR", strategy="increment")	
	private int id;
	
	@Column(name="ciudad", nullable=false, length=255)	
	private String ciudad;
	
	@OneToOne(targetEntity=orm.Multa.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="multaid", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Multa multa;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Atributo obligatorio, ya que no puede existir una solicitud de exorto son la ciudad de cambio.
	 */
	public void setCiudad(String value) {
		this.ciudad = value;
	}
	
	/**
	 * Atributo obligatorio, ya que no puede existir una solicitud de exorto son la ciudad de cambio.
	 */
	public String getCiudad() {
		return ciudad;
	}
	
	public void setMulta(orm.Multa value) {
		if (this.multa != value) {
			orm.Multa lmulta = this.multa;
			this.multa = value;
			if (value != null) {
				multa.setSolicitud_exorto(this);
			}
			else {
				lmulta.setSolicitud_exorto(null);
			}
		}
	}
	
	public orm.Multa getMulta() {
		return multa;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
