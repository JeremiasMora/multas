/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class JuzgadoDAO {
	public static Juzgado loadJuzgadoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadJuzgadoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado getJuzgadoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getJuzgadoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado loadJuzgadoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadJuzgadoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado getJuzgadoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getJuzgadoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado loadJuzgadoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Juzgado) session.load(orm.Juzgado.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado getJuzgadoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Juzgado) session.get(orm.Juzgado.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado loadJuzgadoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Juzgado) session.load(orm.Juzgado.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado getJuzgadoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Juzgado) session.get(orm.Juzgado.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryJuzgado(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return queryJuzgado(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryJuzgado(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return queryJuzgado(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado[] listJuzgadoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listJuzgadoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado[] listJuzgadoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listJuzgadoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryJuzgado(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Juzgado as Juzgado");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryJuzgado(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Juzgado as Juzgado");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Juzgado", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado[] listJuzgadoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryJuzgado(session, condition, orderBy);
			return (Juzgado[]) list.toArray(new Juzgado[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado[] listJuzgadoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryJuzgado(session, condition, orderBy, lockMode);
			return (Juzgado[]) list.toArray(new Juzgado[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado loadJuzgadoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadJuzgadoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado loadJuzgadoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadJuzgadoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado loadJuzgadoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Juzgado[] juzgados = listJuzgadoByQuery(session, condition, orderBy);
		if (juzgados != null && juzgados.length > 0)
			return juzgados[0];
		else
			return null;
	}
	
	public static Juzgado loadJuzgadoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Juzgado[] juzgados = listJuzgadoByQuery(session, condition, orderBy, lockMode);
		if (juzgados != null && juzgados.length > 0)
			return juzgados[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateJuzgadoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateJuzgadoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateJuzgadoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateJuzgadoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateJuzgadoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Juzgado as Juzgado");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateJuzgadoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Juzgado as Juzgado");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Juzgado", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado createJuzgado() {
		return new orm.Juzgado();
	}
	
	public static boolean save(orm.Juzgado juzgado) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().saveObject(juzgado);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Juzgado juzgado) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().deleteObject(juzgado);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Juzgado juzgado)throws PersistentException {
		try {
			if(juzgado.getPersonaid_usuario() != null) {
				juzgado.getPersonaid_usuario().juzgado.remove(juzgado);
			}
			
			orm.Resolucion[] lResolucions = juzgado.resolucion.toArray();
			for(int i = 0; i < lResolucions.length; i++) {
				lResolucions[i].setJuzgado(null);
			}
			return delete(juzgado);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Juzgado juzgado, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(juzgado.getPersonaid_usuario() != null) {
				juzgado.getPersonaid_usuario().juzgado.remove(juzgado);
			}
			
			orm.Resolucion[] lResolucions = juzgado.resolucion.toArray();
			for(int i = 0; i < lResolucions.length; i++) {
				lResolucions[i].setJuzgado(null);
			}
			try {
				session.delete(juzgado);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Juzgado juzgado) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().refresh(juzgado);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Juzgado juzgado) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().evict(juzgado);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Juzgado loadJuzgadoByCriteria(JuzgadoCriteria juzgadoCriteria) {
		Juzgado[] juzgados = listJuzgadoByCriteria(juzgadoCriteria);
		if(juzgados == null || juzgados.length == 0) {
			return null;
		}
		return juzgados[0];
	}
	
	public static Juzgado[] listJuzgadoByCriteria(JuzgadoCriteria juzgadoCriteria) {
		return juzgadoCriteria.listJuzgado();
	}
}
