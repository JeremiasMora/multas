/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
/**
 * Una multa puede tener una solicitud de cambio de fecha, solicitud que la pide un infractor.
 */
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="solicitud_cambio_fecha")
public class Solicitud_cambio_fecha implements Serializable {
	public Solicitud_cambio_fecha() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_SOLICITUD_CAMBIO_FECHA_MULTA) {
			this.multa = (orm.Multa) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_SOLICITUD_CAMBIO_FECHA_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_SOLICITUD_CAMBIO_FECHA_ID_GENERATOR", strategy="increment")	
	private int id;
	
	@Column(name="carta_solicitud", nullable=true, length=255)	
	private String carta_solicitud;
	
	@Column(name="fecha", nullable=true)	
	@Temporal(TemporalType.DATE)	
	private java.util.Date fecha;
	
	@ManyToOne(targetEntity=orm.Multa.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="multaid", referencedColumnName="id", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Multa multa;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setCarta_solicitud(String value) {
		this.carta_solicitud = value;
	}
	
	public String getCarta_solicitud() {
		return carta_solicitud;
	}
	
	public void setFecha(java.util.Date value) {
		this.fecha = value;
	}
	
	public java.util.Date getFecha() {
		return fecha;
	}
	
	public void setMulta(orm.Multa value) {
		if (multa != null) {
			multa.solicitud_cambio_fecha.remove(this);
		}
		if (value != null) {
			value.solicitud_cambio_fecha.add(this);
		}
	}
	
	public orm.Multa getMulta() {
		return multa;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Multa(orm.Multa value) {
		this.multa = value;
	}
	
	private orm.Multa getORM_Multa() {
		return multa;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
