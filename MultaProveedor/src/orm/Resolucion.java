/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
/**
 * Una multa tiene asignada una resolución que es echa por un juez, donde este define el valor a pagar de la multa.
 */
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="resolucion")
public class Resolucion implements Serializable {
	public Resolucion() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_RESOLUCION_JUZGADO) {
			this.juzgado = (orm.Juzgado) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RESOLUCION_MULTA) {
			this.multa = (orm.Multa) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_RESOLUCION_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_RESOLUCION_ID_GENERATOR", strategy="increment")	
	private int id;
	
	@ManyToOne(targetEntity=orm.Juzgado.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="juzgadoid", referencedColumnName="id", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Juzgado juzgado;
	
	@Column(name="valorpagar", nullable=true, length=10)	
	private Integer valorpagar;
	
	@OneToOne(targetEntity=orm.Multa.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="multaid", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Multa multa;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setValorpagar(int value) {
		setValorpagar(new Integer(value));
	}
	
	public void setValorpagar(Integer value) {
		this.valorpagar = value;
	}
	
	public Integer getValorpagar() {
		return valorpagar;
	}
	
	public void setJuzgado(orm.Juzgado value) {
		if (juzgado != null) {
			juzgado.resolucion.remove(this);
		}
		if (value != null) {
			value.resolucion.add(this);
		}
	}
	
	public orm.Juzgado getJuzgado() {
		return juzgado;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Juzgado(orm.Juzgado value) {
		this.juzgado = value;
	}
	
	private orm.Juzgado getORM_Juzgado() {
		return juzgado;
	}
	
	public void setMulta(orm.Multa value) {
		if (this.multa != value) {
			orm.Multa lmulta = this.multa;
			this.multa = value;
			if (value != null) {
				multa.setResolucion(this);
			}
			else {
				lmulta.setResolucion(null);
			}
		}
	}
	
	public orm.Multa getMulta() {
		return multa;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
