/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="token")
public class Token implements Serializable {
	public Token() {
	}
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_TOKEN_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_TOKEN_ID_GENERATOR", strategy="increment")	
	private int id;
	
	@Column(name="token", nullable=true, length=255)	
	private String token;
	
	@Column(name="fecha_inicio", nullable=true, length=255)	
	private String fecha_inicio;
	
	@Column(name="fecha_fin", nullable=true, length=255)	
	private String fecha_fin;
	
	@OneToOne(targetEntity=orm.Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaid_usuario", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Persona personaid_usuario;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setToken(String value) {
		this.token = value;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setFecha_inicio(String value) {
		this.fecha_inicio = value;
	}
	
	public String getFecha_inicio() {
		return fecha_inicio;
	}
	
	public void setFecha_fin(String value) {
		this.fecha_fin = value;
	}
	
	public String getFecha_fin() {
		return fecha_fin;
	}
	
	public void setPersonaid_usuario(orm.Persona value) {
		if (this.personaid_usuario != value) {
			orm.Persona lpersonaid_usuario = this.personaid_usuario;
			this.personaid_usuario = value;
			if (value != null) {
				personaid_usuario.setToken(this);
			}
			else {
				lpersonaid_usuario.setToken(null);
			}
		}
	}
	
	public orm.Persona getPersonaid_usuario() {
		return personaid_usuario;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
