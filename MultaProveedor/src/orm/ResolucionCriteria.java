/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class ResolucionCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final IntegerExpression valorpagar;
	
	public ResolucionCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		valorpagar = new IntegerExpression("valorpagar", this);
	}
	
	public ResolucionCriteria(PersistentSession session) {
		this(session.createCriteria(Resolucion.class));
	}
	
	public ResolucionCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public JuzgadoCriteria createJuzgadoCriteria() {
		return new JuzgadoCriteria(createCriteria("juzgado"));
	}
	
	public MultaCriteria createMultaCriteria() {
		return new MultaCriteria(createCriteria("multa"));
	}
	
	public Resolucion uniqueResolucion() {
		return (Resolucion) super.uniqueResult();
	}
	
	public Resolucion[] listResolucion() {
		java.util.List list = super.list();
		return (Resolucion[]) list.toArray(new Resolucion[list.size()]);
	}
}

