/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Solicitud_exortoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression ciudad;
	
	public Solicitud_exortoDetachedCriteria() {
		super(orm.Solicitud_exorto.class, orm.Solicitud_exortoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		ciudad = new StringExpression("ciudad", this.getDetachedCriteria());
	}
	
	public Solicitud_exortoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Solicitud_exortoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		ciudad = new StringExpression("ciudad", this.getDetachedCriteria());
	}
	
	public MultaDetachedCriteria createMultaCriteria() {
		return new MultaDetachedCriteria(createCriteria("multa"));
	}
	
	public Solicitud_exorto uniqueSolicitud_exorto(PersistentSession session) {
		return (Solicitud_exorto) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Solicitud_exorto[] listSolicitud_exorto(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Solicitud_exorto[]) list.toArray(new Solicitud_exorto[list.size()]);
	}
}

