/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TokenCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression token;
	public final StringExpression fecha_inicio;
	public final StringExpression fecha_fin;
	
	public TokenCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		token = new StringExpression("token", this);
		fecha_inicio = new StringExpression("fecha_inicio", this);
		fecha_fin = new StringExpression("fecha_fin", this);
	}
	
	public TokenCriteria(PersistentSession session) {
		this(session.createCriteria(Token.class));
	}
	
	public TokenCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public PersonaCriteria createPersonaid_usuarioCriteria() {
		return new PersonaCriteria(createCriteria("personaid_usuario"));
	}
	
	public Token uniqueToken() {
		return (Token) super.uniqueResult();
	}
	
	public Token[] listToken() {
		java.util.List list = super.list();
		return (Token[]) list.toArray(new Token[list.size()]);
	}
}

