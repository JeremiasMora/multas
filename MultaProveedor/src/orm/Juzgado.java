/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="juzgado")
public class Juzgado implements Serializable {
	public Juzgado() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_JUZGADO_RESOLUCION) {
			return ORM_resolucion;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_JUZGADO_PERSONAID_USUARIO) {
			this.personaid_usuario = (orm.Persona) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_JUZGADO_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_JUZGADO_ID_GENERATOR", strategy="increment")	
	private int id;
	
	@ManyToOne(targetEntity=orm.Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaid_usuario", referencedColumnName="id_usuario", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Persona personaid_usuario;
	
	@OneToMany(mappedBy="juzgado", targetEntity=orm.Resolucion.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_resolucion = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setPersonaid_usuario(orm.Persona value) {
		if (personaid_usuario != null) {
			personaid_usuario.juzgado.remove(this);
		}
		if (value != null) {
			value.juzgado.add(this);
		}
	}
	
	public orm.Persona getPersonaid_usuario() {
		return personaid_usuario;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Personaid_usuario(orm.Persona value) {
		this.personaid_usuario = value;
	}
	
	private orm.Persona getORM_Personaid_usuario() {
		return personaid_usuario;
	}
	
	private void setORM_Resolucion(java.util.Set value) {
		this.ORM_resolucion = value;
	}
	
	private java.util.Set getORM_Resolucion() {
		return ORM_resolucion;
	}
	
	@Transient	
	public final orm.ResolucionSetCollection resolucion = new orm.ResolucionSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_JUZGADO_RESOLUCION, orm.ORMConstants.KEY_RESOLUCION_JUZGADO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
