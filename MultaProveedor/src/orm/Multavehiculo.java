/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
/**
 * Tabla que enlaza vehículos con Multas, ya que es una relación de muchos a muchos
 */
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="multavehiculo")
public class Multavehiculo implements Serializable {
	public Multavehiculo() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_MULTAVEHICULO_VEHICULOSID_VEHICULO) {
			this.vehiculosid_vehiculo = (orm.Vehiculos) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_MULTAVEHICULO_MULTA) {
			this.multa = (orm.Multa) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_MULTAVEHICULO_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_MULTAVEHICULO_ID_GENERATOR", strategy="native")	
	private int id;
	
	@ManyToOne(targetEntity=orm.Vehiculos.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="vehiculosid_vehiculo", referencedColumnName="id_vehiculo", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Vehiculos vehiculosid_vehiculo;
	
	@OneToOne(targetEntity=orm.Multa.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="multaid", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Multa multa;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setVehiculosid_vehiculo(orm.Vehiculos value) {
		if (vehiculosid_vehiculo != null) {
			vehiculosid_vehiculo.multavehiculo.remove(this);
		}
		if (value != null) {
			value.multavehiculo.add(this);
		}
	}
	
	public orm.Vehiculos getVehiculosid_vehiculo() {
		return vehiculosid_vehiculo;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Vehiculosid_vehiculo(orm.Vehiculos value) {
		this.vehiculosid_vehiculo = value;
	}
	
	private orm.Vehiculos getORM_Vehiculosid_vehiculo() {
		return vehiculosid_vehiculo;
	}
	
	public void setMulta(orm.Multa value) {
		if (this.multa != value) {
			orm.Multa lmulta = this.multa;
			this.multa = value;
			if (value != null) {
				multa.setMultavehiculo(this);
			}
			else {
				lmulta.setMultavehiculo(null);
			}
		}
	}
	
	public orm.Multa getMulta() {
		return multa;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
