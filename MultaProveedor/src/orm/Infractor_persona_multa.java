/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="infractor_persona_multa")
public class Infractor_persona_multa implements Serializable {
	public Infractor_persona_multa() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_INFRACTOR_PERSONA_MULTA_INFRACTOR_PERSONA) {
			this.infractor_persona = (orm.Infractor_persona) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_INFRACTOR_PERSONA_MULTA_MULTA) {
			this.multa = (orm.Multa) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_INFRACTOR_PERSONA_MULTA_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_INFRACTOR_PERSONA_MULTA_ID_GENERATOR", strategy="increment")	
	private int id;
	
	@ManyToOne(targetEntity=orm.Infractor_persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="infractor_personaid", referencedColumnName="id", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Infractor_persona infractor_persona;
	
	@ManyToOne(targetEntity=orm.Multa.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="multaid", referencedColumnName="id", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Multa multa;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setInfractor_persona(orm.Infractor_persona value) {
		if (infractor_persona != null) {
			infractor_persona.infractor_persona_multa.remove(this);
		}
		if (value != null) {
			value.infractor_persona_multa.add(this);
		}
	}
	
	public orm.Infractor_persona getInfractor_persona() {
		return infractor_persona;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Infractor_persona(orm.Infractor_persona value) {
		this.infractor_persona = value;
	}
	
	private orm.Infractor_persona getORM_Infractor_persona() {
		return infractor_persona;
	}
	
	public void setMulta(orm.Multa value) {
		if (multa != null) {
			multa.infractor_persona_multa.remove(this);
		}
		if (value != null) {
			value.infractor_persona_multa.add(this);
		}
	}
	
	public orm.Multa getMulta() {
		return multa;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Multa(orm.Multa value) {
		this.multa = value;
	}
	
	private orm.Multa getORM_Multa() {
		return multa;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
