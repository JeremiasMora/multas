/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Solicitud_cambio_fechaDAO {
	public static Solicitud_cambio_fecha loadSolicitud_cambio_fechaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadSolicitud_cambio_fechaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha getSolicitud_cambio_fechaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getSolicitud_cambio_fechaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha loadSolicitud_cambio_fechaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadSolicitud_cambio_fechaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha getSolicitud_cambio_fechaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getSolicitud_cambio_fechaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha loadSolicitud_cambio_fechaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Solicitud_cambio_fecha) session.load(orm.Solicitud_cambio_fecha.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha getSolicitud_cambio_fechaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Solicitud_cambio_fecha) session.get(orm.Solicitud_cambio_fecha.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha loadSolicitud_cambio_fechaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Solicitud_cambio_fecha) session.load(orm.Solicitud_cambio_fecha.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha getSolicitud_cambio_fechaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Solicitud_cambio_fecha) session.get(orm.Solicitud_cambio_fecha.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySolicitud_cambio_fecha(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return querySolicitud_cambio_fecha(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySolicitud_cambio_fecha(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return querySolicitud_cambio_fecha(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha[] listSolicitud_cambio_fechaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listSolicitud_cambio_fechaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha[] listSolicitud_cambio_fechaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listSolicitud_cambio_fechaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySolicitud_cambio_fecha(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Solicitud_cambio_fecha as Solicitud_cambio_fecha");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySolicitud_cambio_fecha(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Solicitud_cambio_fecha as Solicitud_cambio_fecha");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Solicitud_cambio_fecha", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha[] listSolicitud_cambio_fechaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = querySolicitud_cambio_fecha(session, condition, orderBy);
			return (Solicitud_cambio_fecha[]) list.toArray(new Solicitud_cambio_fecha[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha[] listSolicitud_cambio_fechaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = querySolicitud_cambio_fecha(session, condition, orderBy, lockMode);
			return (Solicitud_cambio_fecha[]) list.toArray(new Solicitud_cambio_fecha[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha loadSolicitud_cambio_fechaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadSolicitud_cambio_fechaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha loadSolicitud_cambio_fechaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadSolicitud_cambio_fechaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha loadSolicitud_cambio_fechaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Solicitud_cambio_fecha[] solicitud_cambio_fechas = listSolicitud_cambio_fechaByQuery(session, condition, orderBy);
		if (solicitud_cambio_fechas != null && solicitud_cambio_fechas.length > 0)
			return solicitud_cambio_fechas[0];
		else
			return null;
	}
	
	public static Solicitud_cambio_fecha loadSolicitud_cambio_fechaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Solicitud_cambio_fecha[] solicitud_cambio_fechas = listSolicitud_cambio_fechaByQuery(session, condition, orderBy, lockMode);
		if (solicitud_cambio_fechas != null && solicitud_cambio_fechas.length > 0)
			return solicitud_cambio_fechas[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateSolicitud_cambio_fechaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateSolicitud_cambio_fechaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateSolicitud_cambio_fechaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateSolicitud_cambio_fechaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateSolicitud_cambio_fechaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Solicitud_cambio_fecha as Solicitud_cambio_fecha");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateSolicitud_cambio_fechaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Solicitud_cambio_fecha as Solicitud_cambio_fecha");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Solicitud_cambio_fecha", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha createSolicitud_cambio_fecha() {
		return new orm.Solicitud_cambio_fecha();
	}
	
	public static boolean save(orm.Solicitud_cambio_fecha solicitud_cambio_fecha) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().saveObject(solicitud_cambio_fecha);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Solicitud_cambio_fecha solicitud_cambio_fecha) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().deleteObject(solicitud_cambio_fecha);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Solicitud_cambio_fecha solicitud_cambio_fecha)throws PersistentException {
		try {
			if(solicitud_cambio_fecha.getMulta() != null) {
				solicitud_cambio_fecha.getMulta().solicitud_cambio_fecha.remove(solicitud_cambio_fecha);
			}
			
			return delete(solicitud_cambio_fecha);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Solicitud_cambio_fecha solicitud_cambio_fecha, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(solicitud_cambio_fecha.getMulta() != null) {
				solicitud_cambio_fecha.getMulta().solicitud_cambio_fecha.remove(solicitud_cambio_fecha);
			}
			
			try {
				session.delete(solicitud_cambio_fecha);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Solicitud_cambio_fecha solicitud_cambio_fecha) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().refresh(solicitud_cambio_fecha);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Solicitud_cambio_fecha solicitud_cambio_fecha) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().evict(solicitud_cambio_fecha);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_cambio_fecha loadSolicitud_cambio_fechaByCriteria(Solicitud_cambio_fechaCriteria solicitud_cambio_fechaCriteria) {
		Solicitud_cambio_fecha[] solicitud_cambio_fechas = listSolicitud_cambio_fechaByCriteria(solicitud_cambio_fechaCriteria);
		if(solicitud_cambio_fechas == null || solicitud_cambio_fechas.length == 0) {
			return null;
		}
		return solicitud_cambio_fechas[0];
	}
	
	public static Solicitud_cambio_fecha[] listSolicitud_cambio_fechaByCriteria(Solicitud_cambio_fechaCriteria solicitud_cambio_fechaCriteria) {
		return solicitud_cambio_fechaCriteria.listSolicitud_cambio_fecha();
	}
}
