/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Solicitud_cambio_fechaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression carta_solicitud;
	public final DateExpression fecha;
	
	public Solicitud_cambio_fechaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		carta_solicitud = new StringExpression("carta_solicitud", this);
		fecha = new DateExpression("fecha", this);
	}
	
	public Solicitud_cambio_fechaCriteria(PersistentSession session) {
		this(session.createCriteria(Solicitud_cambio_fecha.class));
	}
	
	public Solicitud_cambio_fechaCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public MultaCriteria createMultaCriteria() {
		return new MultaCriteria(createCriteria("multa"));
	}
	
	public Solicitud_cambio_fecha uniqueSolicitud_cambio_fecha() {
		return (Solicitud_cambio_fecha) super.uniqueResult();
	}
	
	public Solicitud_cambio_fecha[] listSolicitud_cambio_fecha() {
		java.util.List list = super.list();
		return (Solicitud_cambio_fecha[]) list.toArray(new Solicitud_cambio_fecha[list.size()]);
	}
}

