/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Infractor_persona_multaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	
	public Infractor_persona_multaDetachedCriteria() {
		super(orm.Infractor_persona_multa.class, orm.Infractor_persona_multaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public Infractor_persona_multaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Infractor_persona_multaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
	}
	
	public Infractor_personaDetachedCriteria createInfractor_personaCriteria() {
		return new Infractor_personaDetachedCriteria(createCriteria("infractor_persona"));
	}
	
	public MultaDetachedCriteria createMultaCriteria() {
		return new MultaDetachedCriteria(createCriteria("multa"));
	}
	
	public Infractor_persona_multa uniqueInfractor_persona_multa(PersistentSession session) {
		return (Infractor_persona_multa) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Infractor_persona_multa[] listInfractor_persona_multa(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Infractor_persona_multa[]) list.toArray(new Infractor_persona_multa[list.size()]);
	}
}

