/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MultaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression nombre_multa;
	public final StringExpression descripcion_multa;
	public final StringExpression quien_registro_multa;
	public final StringExpression quien_modifico_multa;
	public final StringExpression fecha_creacion;
	
	public MultaDetachedCriteria() {
		super(orm.Multa.class, orm.MultaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		nombre_multa = new StringExpression("nombre_multa", this.getDetachedCriteria());
		descripcion_multa = new StringExpression("descripcion_multa", this.getDetachedCriteria());
		quien_registro_multa = new StringExpression("quien_registro_multa", this.getDetachedCriteria());
		quien_modifico_multa = new StringExpression("quien_modifico_multa", this.getDetachedCriteria());
		fecha_creacion = new StringExpression("fecha_creacion", this.getDetachedCriteria());
	}
	
	public MultaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.MultaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		nombre_multa = new StringExpression("nombre_multa", this.getDetachedCriteria());
		descripcion_multa = new StringExpression("descripcion_multa", this.getDetachedCriteria());
		quien_registro_multa = new StringExpression("quien_registro_multa", this.getDetachedCriteria());
		quien_modifico_multa = new StringExpression("quien_modifico_multa", this.getDetachedCriteria());
		fecha_creacion = new StringExpression("fecha_creacion", this.getDetachedCriteria());
	}
	
	public CarabineroDetachedCriteria createCarabineroCriteria() {
		return new CarabineroDetachedCriteria(createCriteria("carabinero"));
	}
	
	public VehiculosDetachedCriteria createVehiculosid_vehiculoCriteria() {
		return new VehiculosDetachedCriteria(createCriteria("ORM_vehiculosid_vehiculo"));
	}
	
	public Solicitud_cambio_fechaDetachedCriteria createSolicitud_cambio_fechaCriteria() {
		return new Solicitud_cambio_fechaDetachedCriteria(createCriteria("ORM_solicitud_cambio_fecha"));
	}
	
	public MultavehiculoDetachedCriteria createMultavehiculoCriteria() {
		return new MultavehiculoDetachedCriteria(createCriteria("multavehiculo"));
	}
	
	public Infractor_persona_multaDetachedCriteria createInfractor_persona_multaCriteria() {
		return new Infractor_persona_multaDetachedCriteria(createCriteria("ORM_infractor_persona_multa"));
	}
	
	public PagoDetachedCriteria createPagoCriteria() {
		return new PagoDetachedCriteria(createCriteria("pago"));
	}
	
	public ResolucionDetachedCriteria createResolucionCriteria() {
		return new ResolucionDetachedCriteria(createCriteria("resolucion"));
	}
	
	public Solicitud_exortoDetachedCriteria createSolicitud_exortoCriteria() {
		return new Solicitud_exortoDetachedCriteria(createCriteria("solicitud_exorto"));
	}
	
	public Multa uniqueMulta(PersistentSession session) {
		return (Multa) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Multa[] listMulta(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Multa[]) list.toArray(new Multa[list.size()]);
	}
}

