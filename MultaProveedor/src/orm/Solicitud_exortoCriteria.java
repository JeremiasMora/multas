/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Solicitud_exortoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression ciudad;
	
	public Solicitud_exortoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		ciudad = new StringExpression("ciudad", this);
	}
	
	public Solicitud_exortoCriteria(PersistentSession session) {
		this(session.createCriteria(Solicitud_exorto.class));
	}
	
	public Solicitud_exortoCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public MultaCriteria createMultaCriteria() {
		return new MultaCriteria(createCriteria("multa"));
	}
	
	public Solicitud_exorto uniqueSolicitud_exorto() {
		return (Solicitud_exorto) super.uniqueResult();
	}
	
	public Solicitud_exorto[] listSolicitud_exorto() {
		java.util.List list = super.list();
		return (Solicitud_exorto[]) list.toArray(new Solicitud_exorto[list.size()]);
	}
}

