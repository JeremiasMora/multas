/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PersonaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id_usuario;
	public final StringExpression rut;
	public final StringExpression pass;
	public final StringExpression nombre;
	public final StringExpression apellido;
	public final StringExpression direccion;
	
	public PersonaCriteria(Criteria criteria) {
		super(criteria);
		id_usuario = new IntegerExpression("id_usuario", this);
		rut = new StringExpression("rut", this);
		pass = new StringExpression("pass", this);
		nombre = new StringExpression("nombre", this);
		apellido = new StringExpression("apellido", this);
		direccion = new StringExpression("direccion", this);
	}
	
	public PersonaCriteria(PersistentSession session) {
		this(session.createCriteria(Persona.class));
	}
	
	public PersonaCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public JuzgadoCriteria createJuzgadoCriteria() {
		return new JuzgadoCriteria(createCriteria("ORM_juzgado"));
	}
	
	public VehiculosCriteria createVehiculosCriteria() {
		return new VehiculosCriteria(createCriteria("ORM_vehiculos"));
	}
	
	public SecretariaCriteria createSecretariaCriteria() {
		return new SecretariaCriteria(createCriteria("secretaria"));
	}
	
	public CarabineroCriteria createCarabineroCriteria() {
		return new CarabineroCriteria(createCriteria("carabinero"));
	}
	
	public TokenCriteria createTokenCriteria() {
		return new TokenCriteria(createCriteria("token"));
	}
	
	public Infractor_personaCriteria createInfractor_personaCriteria() {
		return new Infractor_personaCriteria(createCriteria("infractor_persona"));
	}
	
	public TesoreraCriteria createTesoreraCriteria() {
		return new TesoreraCriteria(createCriteria("tesorera"));
	}
	
	public Persona uniquePersona() {
		return (Persona) super.uniqueResult();
	}
	
	public Persona[] listPersona() {
		java.util.List list = super.list();
		return (Persona[]) list.toArray(new Persona[list.size()]);
	}
}

