/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
/**
 * Todo usuario al momento de cometer una infracción pasa a ser un infractor.
 */
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="infractor_persona")
public class Infractor_persona implements Serializable {
	public Infractor_persona() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_INFRACTOR_PERSONA_INFRACTOR_PERSONA_MULTA) {
			return ORM_infractor_persona_multa;
		}
		
		return null;
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_INFRACTOR_PERSONA_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_INFRACTOR_PERSONA_ID_GENERATOR", strategy="native")	
	private int id;
	
	@OneToOne(targetEntity=orm.Persona.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="personaid_usuario", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Persona personaid_usuario;
	
	@OneToMany(mappedBy="infractor_persona", targetEntity=orm.Infractor_persona_multa.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_infractor_persona_multa = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setPersonaid_usuario(orm.Persona value) {
		if (this.personaid_usuario != value) {
			orm.Persona lpersonaid_usuario = this.personaid_usuario;
			this.personaid_usuario = value;
			if (value != null) {
				personaid_usuario.setInfractor_persona(this);
			}
			else {
				lpersonaid_usuario.setInfractor_persona(null);
			}
		}
	}
	
	public orm.Persona getPersonaid_usuario() {
		return personaid_usuario;
	}
	
	private void setORM_Infractor_persona_multa(java.util.Set value) {
		this.ORM_infractor_persona_multa = value;
	}
	
	private java.util.Set getORM_Infractor_persona_multa() {
		return ORM_infractor_persona_multa;
	}
	
	@Transient	
	public final orm.Infractor_persona_multaSetCollection infractor_persona_multa = new orm.Infractor_persona_multaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_INFRACTOR_PERSONA_INFRACTOR_PERSONA_MULTA, orm.ORMConstants.KEY_INFRACTOR_PERSONA_MULTA_INFRACTOR_PERSONA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
