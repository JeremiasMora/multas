/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class JuzgadoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	
	public JuzgadoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
	}
	
	public JuzgadoCriteria(PersistentSession session) {
		this(session.createCriteria(Juzgado.class));
	}
	
	public JuzgadoCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public PersonaCriteria createPersonaid_usuarioCriteria() {
		return new PersonaCriteria(createCriteria("personaid_usuario"));
	}
	
	public ResolucionCriteria createResolucionCriteria() {
		return new ResolucionCriteria(createCriteria("ORM_resolucion"));
	}
	
	public Juzgado uniqueJuzgado() {
		return (Juzgado) super.uniqueResult();
	}
	
	public Juzgado[] listJuzgado() {
		java.util.List list = super.list();
		return (Juzgado[]) list.toArray(new Juzgado[list.size()]);
	}
}

