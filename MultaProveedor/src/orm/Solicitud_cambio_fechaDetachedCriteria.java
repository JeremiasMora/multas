/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class Solicitud_cambio_fechaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression carta_solicitud;
	public final DateExpression fecha;
	
	public Solicitud_cambio_fechaDetachedCriteria() {
		super(orm.Solicitud_cambio_fecha.class, orm.Solicitud_cambio_fechaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		carta_solicitud = new StringExpression("carta_solicitud", this.getDetachedCriteria());
		fecha = new DateExpression("fecha", this.getDetachedCriteria());
	}
	
	public Solicitud_cambio_fechaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.Solicitud_cambio_fechaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		carta_solicitud = new StringExpression("carta_solicitud", this.getDetachedCriteria());
		fecha = new DateExpression("fecha", this.getDetachedCriteria());
	}
	
	public MultaDetachedCriteria createMultaCriteria() {
		return new MultaDetachedCriteria(createCriteria("multa"));
	}
	
	public Solicitud_cambio_fecha uniqueSolicitud_cambio_fecha(PersistentSession session) {
		return (Solicitud_cambio_fecha) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Solicitud_cambio_fecha[] listSolicitud_cambio_fecha(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Solicitud_cambio_fecha[]) list.toArray(new Solicitud_cambio_fecha[list.size()]);
	}
}

