/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PagoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression formapago;
	
	public PagoDetachedCriteria() {
		super(orm.Pago.class, orm.PagoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		formapago = new StringExpression("formapago", this.getDetachedCriteria());
	}
	
	public PagoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.PagoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		formapago = new StringExpression("formapago", this.getDetachedCriteria());
	}
	
	public TesoreraDetachedCriteria createTesoreraCriteria() {
		return new TesoreraDetachedCriteria(createCriteria("tesorera"));
	}
	
	public MultaDetachedCriteria createMultaCriteria() {
		return new MultaDetachedCriteria(createCriteria("multa"));
	}
	
	public Pago uniquePago(PersistentSession session) {
		return (Pago) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Pago[] listPago(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Pago[]) list.toArray(new Pago[list.size()]);
	}
}

