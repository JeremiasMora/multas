/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
/**
 * Esta tabla almacena la información de la multa cursada tanto a un infractor como a un vehiculo.
 */
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="multa")
public class Multa implements Serializable {
	public Multa() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_MULTA_VEHICULOSID_VEHICULO) {
			return ORM_vehiculosid_vehiculo;
		}
		else if (key == orm.ORMConstants.KEY_MULTA_SOLICITUD_CAMBIO_FECHA) {
			return ORM_solicitud_cambio_fecha;
		}
		else if (key == orm.ORMConstants.KEY_MULTA_INFRACTOR_PERSONA_MULTA) {
			return ORM_infractor_persona_multa;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_MULTA_CARABINERO) {
			this.carabinero = (orm.Carabinero) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_MULTA_MULTAVEHICULO) {
			this.multavehiculo = (orm.Multavehiculo) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_MULTA_PAGO) {
			this.pago = (orm.Pago) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_MULTA_RESOLUCION) {
			this.resolucion = (orm.Resolucion) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_MULTA_SOLICITUD_EXORTO) {
			this.solicitud_exorto = (orm.Solicitud_exorto) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_MULTA_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_MULTA_ID_GENERATOR", strategy="increment")	
	private int id;
	
	@Column(name="nombre_multa", nullable=true, length=255)	
	private String nombre_multa;
	
	@Column(name="descripcion_multa", nullable=true, length=255)	
	private String descripcion_multa;
	
	@Column(name="quien_registro_multa", nullable=true, length=255)	
	private String quien_registro_multa;
	
	@Column(name="quien_modifico_multa", nullable=true, length=255)	
	private String quien_modifico_multa;
	
	@Column(name="fecha_creacion", nullable=true, length=255)	
	private String fecha_creacion;
	
	@Column(name="desactivar", nullable=true, length=1)	
	private Character desactivar;
	
	@ManyToOne(targetEntity=orm.Carabinero.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="carabineroid", referencedColumnName="id") })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Carabinero carabinero;
	
	@ManyToMany(targetEntity=orm.Vehiculos.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinTable(name="vehiculos_multa", joinColumns={ @JoinColumn(name="multaid") }, inverseJoinColumns={ @JoinColumn(name="vehiculosid_vehiculo") })	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_vehiculosid_vehiculo = new java.util.HashSet();
	
	@OneToMany(mappedBy="multa", targetEntity=orm.Solicitud_cambio_fecha.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_solicitud_cambio_fecha = new java.util.HashSet();
	
	@OneToOne(mappedBy="multa", targetEntity=orm.Multavehiculo.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Multavehiculo multavehiculo;
	
	@OneToMany(mappedBy="multa", targetEntity=orm.Infractor_persona_multa.class)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyCollection(org.hibernate.annotations.LazyCollectionOption.TRUE)	
	private java.util.Set ORM_infractor_persona_multa = new java.util.HashSet();
	
	@OneToOne(mappedBy="multa", targetEntity=orm.Pago.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Pago pago;
	
	@OneToOne(mappedBy="multa", targetEntity=orm.Resolucion.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Resolucion resolucion;
	
	@OneToOne(mappedBy="multa", targetEntity=orm.Solicitud_exorto.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Solicitud_exorto solicitud_exorto;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setNombre_multa(String value) {
		this.nombre_multa = value;
	}
	
	public String getNombre_multa() {
		return nombre_multa;
	}
	
	/**
	 * descripción de la multa
	 */
	public void setDescripcion_multa(String value) {
		this.descripcion_multa = value;
	}
	
	/**
	 * descripción de la multa
	 */
	public String getDescripcion_multa() {
		return descripcion_multa;
	}
	
	/**
	 * Nombre de carabinero que creó el registro o multa
	 */
	public void setQuien_registro_multa(String value) {
		this.quien_registro_multa = value;
	}
	
	/**
	 * Nombre de carabinero que creó el registro o multa
	 */
	public String getQuien_registro_multa() {
		return quien_registro_multa;
	}
	
	/**
	 * Nombre del carabinero que modificó la multa
	 */
	public void setQuien_modifico_multa(String value) {
		this.quien_modifico_multa = value;
	}
	
	/**
	 * Nombre del carabinero que modificó la multa
	 */
	public String getQuien_modifico_multa() {
		return quien_modifico_multa;
	}
	
	public void setFecha_creacion(String value) {
		this.fecha_creacion = value;
	}
	
	public String getFecha_creacion() {
		return fecha_creacion;
	}
	
	/**
	 *  Campo desactivar cuyo valor es cero o uno. De esta manera el eliminar solo modifica este valor.
	 */
	public void setDesactivar(char value) {
		setDesactivar(new Character(value));
	}
	
	/**
	 *  Campo desactivar cuyo valor es cero o uno. De esta manera el eliminar solo modifica este valor.
	 */
	public void setDesactivar(Character value) {
		this.desactivar = value;
	}
	
	/**
	 *  Campo desactivar cuyo valor es cero o uno. De esta manera el eliminar solo modifica este valor.
	 */
	public Character getDesactivar() {
		return desactivar;
	}
	
	private void setORM_Vehiculosid_vehiculo(java.util.Set value) {
		this.ORM_vehiculosid_vehiculo = value;
	}
	
	private java.util.Set getORM_Vehiculosid_vehiculo() {
		return ORM_vehiculosid_vehiculo;
	}
	
	@Transient	
	public final orm.VehiculosSetCollection vehiculosid_vehiculo = new orm.VehiculosSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_MULTA_VEHICULOSID_VEHICULO, orm.ORMConstants.KEY_VEHICULOS_MULTA, orm.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public void setCarabinero(orm.Carabinero value) {
		if (carabinero != null) {
			carabinero.multa.remove(this);
		}
		if (value != null) {
			value.multa.add(this);
		}
	}
	
	public orm.Carabinero getCarabinero() {
		return carabinero;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Carabinero(orm.Carabinero value) {
		this.carabinero = value;
	}
	
	private orm.Carabinero getORM_Carabinero() {
		return carabinero;
	}
	
	private void setORM_Solicitud_cambio_fecha(java.util.Set value) {
		this.ORM_solicitud_cambio_fecha = value;
	}
	
	private java.util.Set getORM_Solicitud_cambio_fecha() {
		return ORM_solicitud_cambio_fecha;
	}
	
	@Transient	
	public final orm.Solicitud_cambio_fechaSetCollection solicitud_cambio_fecha = new orm.Solicitud_cambio_fechaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_MULTA_SOLICITUD_CAMBIO_FECHA, orm.ORMConstants.KEY_SOLICITUD_CAMBIO_FECHA_MULTA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public void setMultavehiculo(orm.Multavehiculo value) {
		if (this.multavehiculo != value) {
			orm.Multavehiculo lmultavehiculo = this.multavehiculo;
			this.multavehiculo = value;
			if (value != null) {
				multavehiculo.setMulta(this);
			}
			else {
				lmultavehiculo.setMulta(null);
			}
		}
	}
	
	public orm.Multavehiculo getMultavehiculo() {
		return multavehiculo;
	}
	
	private void setORM_Infractor_persona_multa(java.util.Set value) {
		this.ORM_infractor_persona_multa = value;
	}
	
	private java.util.Set getORM_Infractor_persona_multa() {
		return ORM_infractor_persona_multa;
	}
	
	@Transient	
	public final orm.Infractor_persona_multaSetCollection infractor_persona_multa = new orm.Infractor_persona_multaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_MULTA_INFRACTOR_PERSONA_MULTA, orm.ORMConstants.KEY_INFRACTOR_PERSONA_MULTA_MULTA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public void setPago(orm.Pago value) {
		if (this.pago != value) {
			orm.Pago lpago = this.pago;
			this.pago = value;
			if (value != null) {
				pago.setMulta(this);
			}
			else {
				lpago.setMulta(null);
			}
		}
	}
	
	public orm.Pago getPago() {
		return pago;
	}
	
	public void setResolucion(orm.Resolucion value) {
		if (this.resolucion != value) {
			orm.Resolucion lresolucion = this.resolucion;
			this.resolucion = value;
			if (value != null) {
				resolucion.setMulta(this);
			}
			else {
				lresolucion.setMulta(null);
			}
		}
	}
	
	public orm.Resolucion getResolucion() {
		return resolucion;
	}
	
	public void setSolicitud_exorto(orm.Solicitud_exorto value) {
		if (this.solicitud_exorto != value) {
			orm.Solicitud_exorto lsolicitud_exorto = this.solicitud_exorto;
			this.solicitud_exorto = value;
			if (value != null) {
				solicitud_exorto.setMulta(this);
			}
			else {
				lsolicitud_exorto.setMulta(null);
			}
		}
	}
	
	public orm.Solicitud_exorto getSolicitud_exorto() {
		return solicitud_exorto;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
