/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MultaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression nombre_multa;
	public final StringExpression descripcion_multa;
	public final StringExpression quien_registro_multa;
	public final StringExpression quien_modifico_multa;
	public final StringExpression fecha_creacion;
	
	public MultaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		nombre_multa = new StringExpression("nombre_multa", this);
		descripcion_multa = new StringExpression("descripcion_multa", this);
		quien_registro_multa = new StringExpression("quien_registro_multa", this);
		quien_modifico_multa = new StringExpression("quien_modifico_multa", this);
		fecha_creacion = new StringExpression("fecha_creacion", this);
	}
	
	public MultaCriteria(PersistentSession session) {
		this(session.createCriteria(Multa.class));
	}
	
	public MultaCriteria() throws PersistentException {
		this(orm.DiagramadelaBDMULTASPersistentManager.instance().getSession());
	}
	
	public CarabineroCriteria createCarabineroCriteria() {
		return new CarabineroCriteria(createCriteria("carabinero"));
	}
	
	public VehiculosCriteria createVehiculosid_vehiculoCriteria() {
		return new VehiculosCriteria(createCriteria("ORM_vehiculosid_vehiculo"));
	}
	
	public Solicitud_cambio_fechaCriteria createSolicitud_cambio_fechaCriteria() {
		return new Solicitud_cambio_fechaCriteria(createCriteria("ORM_solicitud_cambio_fecha"));
	}
	
	public MultavehiculoCriteria createMultavehiculoCriteria() {
		return new MultavehiculoCriteria(createCriteria("multavehiculo"));
	}
	
	public Infractor_persona_multaCriteria createInfractor_persona_multaCriteria() {
		return new Infractor_persona_multaCriteria(createCriteria("ORM_infractor_persona_multa"));
	}
	
	public PagoCriteria createPagoCriteria() {
		return new PagoCriteria(createCriteria("pago"));
	}
	
	public ResolucionCriteria createResolucionCriteria() {
		return new ResolucionCriteria(createCriteria("resolucion"));
	}
	
	public Solicitud_exortoCriteria createSolicitud_exortoCriteria() {
		return new Solicitud_exortoCriteria(createCriteria("solicitud_exorto"));
	}
	
	public Multa uniqueMulta() {
		return (Multa) super.uniqueResult();
	}
	
	public Multa[] listMulta() {
		java.util.List list = super.list();
		return (Multa[]) list.toArray(new Multa[list.size()]);
	}
}

