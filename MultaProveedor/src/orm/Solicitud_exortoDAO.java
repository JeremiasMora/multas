/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Solicitud_exortoDAO {
	public static Solicitud_exorto loadSolicitud_exortoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadSolicitud_exortoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto getSolicitud_exortoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getSolicitud_exortoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto loadSolicitud_exortoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadSolicitud_exortoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto getSolicitud_exortoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return getSolicitud_exortoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto loadSolicitud_exortoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Solicitud_exorto) session.load(orm.Solicitud_exorto.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto getSolicitud_exortoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Solicitud_exorto) session.get(orm.Solicitud_exorto.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto loadSolicitud_exortoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Solicitud_exorto) session.load(orm.Solicitud_exorto.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto getSolicitud_exortoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Solicitud_exorto) session.get(orm.Solicitud_exorto.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySolicitud_exorto(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return querySolicitud_exorto(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySolicitud_exorto(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return querySolicitud_exorto(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto[] listSolicitud_exortoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listSolicitud_exortoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto[] listSolicitud_exortoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return listSolicitud_exortoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySolicitud_exorto(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Solicitud_exorto as Solicitud_exorto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List querySolicitud_exorto(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Solicitud_exorto as Solicitud_exorto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Solicitud_exorto", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto[] listSolicitud_exortoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = querySolicitud_exorto(session, condition, orderBy);
			return (Solicitud_exorto[]) list.toArray(new Solicitud_exorto[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto[] listSolicitud_exortoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = querySolicitud_exorto(session, condition, orderBy, lockMode);
			return (Solicitud_exorto[]) list.toArray(new Solicitud_exorto[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto loadSolicitud_exortoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadSolicitud_exortoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto loadSolicitud_exortoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return loadSolicitud_exortoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto loadSolicitud_exortoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Solicitud_exorto[] solicitud_exortos = listSolicitud_exortoByQuery(session, condition, orderBy);
		if (solicitud_exortos != null && solicitud_exortos.length > 0)
			return solicitud_exortos[0];
		else
			return null;
	}
	
	public static Solicitud_exorto loadSolicitud_exortoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Solicitud_exorto[] solicitud_exortos = listSolicitud_exortoByQuery(session, condition, orderBy, lockMode);
		if (solicitud_exortos != null && solicitud_exortos.length > 0)
			return solicitud_exortos[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateSolicitud_exortoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateSolicitud_exortoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateSolicitud_exortoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession();
			return iterateSolicitud_exortoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateSolicitud_exortoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Solicitud_exorto as Solicitud_exorto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateSolicitud_exortoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Solicitud_exorto as Solicitud_exorto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Solicitud_exorto", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto createSolicitud_exorto() {
		return new orm.Solicitud_exorto();
	}
	
	public static boolean save(orm.Solicitud_exorto solicitud_exorto) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().saveObject(solicitud_exorto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Solicitud_exorto solicitud_exorto) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().deleteObject(solicitud_exorto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Solicitud_exorto solicitud_exorto)throws PersistentException {
		try {
			if(solicitud_exorto.getMulta() != null) {
				solicitud_exorto.getMulta().setSolicitud_exorto(null);
			}
			
			return delete(solicitud_exorto);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Solicitud_exorto solicitud_exorto, org.orm.PersistentSession session)throws PersistentException {
		try {
			if(solicitud_exorto.getMulta() != null) {
				solicitud_exorto.getMulta().setSolicitud_exorto(null);
			}
			
			try {
				session.delete(solicitud_exorto);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Solicitud_exorto solicitud_exorto) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().refresh(solicitud_exorto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Solicitud_exorto solicitud_exorto) throws PersistentException {
		try {
			orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().evict(solicitud_exorto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Solicitud_exorto loadSolicitud_exortoByCriteria(Solicitud_exortoCriteria solicitud_exortoCriteria) {
		Solicitud_exorto[] solicitud_exortos = listSolicitud_exortoByCriteria(solicitud_exortoCriteria);
		if(solicitud_exortos == null || solicitud_exortos.length == 0) {
			return null;
		}
		return solicitud_exortos[0];
	}
	
	public static Solicitud_exorto[] listSolicitud_exortoByCriteria(Solicitud_exortoCriteria solicitud_exortoCriteria) {
		return solicitud_exortoCriteria.listSolicitud_exorto();
	}
}
