/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package orm;

import java.io.Serializable;
import javax.persistence.*;
/**
 * Esta tabla almacena la forma de pago que usará el infractor, para cancelar la multa.
 */
@Entity
@org.hibernate.annotations.Proxy(lazy=false)
@Table(name="pago")
public class Pago implements Serializable {
	public Pago() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_PAGO_TESORERA) {
			this.tesorera = (orm.Tesorera) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_PAGO_MULTA) {
			this.multa = (orm.Multa) owner;
		}
	}
	
	@Transient	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	@Column(name="id", nullable=false)	
	@Id	
	@GeneratedValue(generator="ORM_PAGO_ID_GENERATOR")	
	@org.hibernate.annotations.GenericGenerator(name="ORM_PAGO_ID_GENERATOR", strategy="native")	
	private int id;
	
	@Column(name="formapago", nullable=false, length=100)	
	private String formapago;
	
	@ManyToOne(targetEntity=orm.Tesorera.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="tesoreraid", referencedColumnName="id") })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Tesorera tesorera;
	
	@OneToOne(targetEntity=orm.Multa.class, fetch=FetchType.LAZY)	
	@org.hibernate.annotations.Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.LOCK})	
	@JoinColumns({ @JoinColumn(name="multaid", nullable=false) })	
	@org.hibernate.annotations.LazyToOne(value=org.hibernate.annotations.LazyToOneOption.NO_PROXY)	
	private orm.Multa multa;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setFormapago(String value) {
		this.formapago = value;
	}
	
	public String getFormapago() {
		return formapago;
	}
	
	public void setTesorera(orm.Tesorera value) {
		if (tesorera != null) {
			tesorera.pago.remove(this);
		}
		if (value != null) {
			value.pago.add(this);
		}
	}
	
	public orm.Tesorera getTesorera() {
		return tesorera;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Tesorera(orm.Tesorera value) {
		this.tesorera = value;
	}
	
	private orm.Tesorera getORM_Tesorera() {
		return tesorera;
	}
	
	public void setMulta(orm.Multa value) {
		if (this.multa != value) {
			orm.Multa lmulta = this.multa;
			this.multa = value;
			if (value != null) {
				multa.setPago(this);
			}
			else {
				lmulta.setPago(null);
			}
		}
	}
	
	public orm.Multa getMulta() {
		return multa;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
