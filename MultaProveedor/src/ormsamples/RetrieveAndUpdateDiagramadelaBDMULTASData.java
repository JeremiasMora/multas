/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateDiagramadelaBDMULTASData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Carabinero lormCarabinero = orm.CarabineroDAO.loadCarabineroByQuery(null, null);
			// Update the properties of the persistent object
			orm.CarabineroDAO.save(lormCarabinero);
			orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByQuery(null, null);
			// Update the properties of the persistent object
			orm.PersonaDAO.save(lormPersona);
			orm.Multa lormMulta = orm.MultaDAO.loadMultaByQuery(null, null);
			// Update the properties of the persistent object
			orm.MultaDAO.save(lormMulta);
			orm.Solicitud_exorto lormSolicitud_exorto = orm.Solicitud_exortoDAO.loadSolicitud_exortoByQuery(null, null);
			// Update the properties of the persistent object
			orm.Solicitud_exortoDAO.save(lormSolicitud_exorto);
			orm.Solicitud_cambio_fecha lormSolicitud_cambio_fecha = orm.Solicitud_cambio_fechaDAO.loadSolicitud_cambio_fechaByQuery(null, null);
			// Update the properties of the persistent object
			orm.Solicitud_cambio_fechaDAO.save(lormSolicitud_cambio_fecha);
			orm.Vehiculos lormVehiculos = orm.VehiculosDAO.loadVehiculosByQuery(null, null);
			// Update the properties of the persistent object
			orm.VehiculosDAO.save(lormVehiculos);
			orm.Secretaria lormSecretaria = orm.SecretariaDAO.loadSecretariaByQuery(null, null);
			// Update the properties of the persistent object
			orm.SecretariaDAO.save(lormSecretaria);
			orm.Juzgado lormJuzgado = orm.JuzgadoDAO.loadJuzgadoByQuery(null, null);
			// Update the properties of the persistent object
			orm.JuzgadoDAO.save(lormJuzgado);
			orm.Tesorera lormTesorera = orm.TesoreraDAO.loadTesoreraByQuery(null, null);
			// Update the properties of the persistent object
			orm.TesoreraDAO.save(lormTesorera);
			orm.Infractor_persona lormInfractor_persona = orm.Infractor_personaDAO.loadInfractor_personaByQuery(null, null);
			// Update the properties of the persistent object
			orm.Infractor_personaDAO.save(lormInfractor_persona);
			orm.Multavehiculo lormMultavehiculo = orm.MultavehiculoDAO.loadMultavehiculoByQuery(null, null);
			// Update the properties of the persistent object
			orm.MultavehiculoDAO.save(lormMultavehiculo);
			orm.Resolucion lormResolucion = orm.ResolucionDAO.loadResolucionByQuery(null, null);
			// Update the properties of the persistent object
			orm.ResolucionDAO.save(lormResolucion);
			orm.Pago lormPago = orm.PagoDAO.loadPagoByQuery(null, null);
			// Update the properties of the persistent object
			orm.PagoDAO.save(lormPago);
			orm.Token lormToken = orm.TokenDAO.loadTokenByQuery(null, null);
			// Update the properties of the persistent object
			orm.TokenDAO.save(lormToken);
			orm.Infractor_persona_multa lormInfractor_persona_multa = orm.Infractor_persona_multaDAO.loadInfractor_persona_multaByQuery(null, null);
			// Update the properties of the persistent object
			orm.Infractor_persona_multaDAO.save(lormInfractor_persona_multa);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public void retrieveByCriteria() throws PersistentException {
		System.out.println("Retrieving Carabinero by CarabineroCriteria");
		orm.CarabineroCriteria lormCarabineroCriteria = new orm.CarabineroCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormCarabineroCriteria.id.eq();
		System.out.println(lormCarabineroCriteria.uniqueCarabinero());
		
		System.out.println("Retrieving Persona by PersonaCriteria");
		orm.PersonaCriteria lormPersonaCriteria = new orm.PersonaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormPersonaCriteria.id_usuario.eq();
		System.out.println(lormPersonaCriteria.uniquePersona());
		
		System.out.println("Retrieving Multa by MultaCriteria");
		orm.MultaCriteria lormMultaCriteria = new orm.MultaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormMultaCriteria.id.eq();
		System.out.println(lormMultaCriteria.uniqueMulta());
		
		System.out.println("Retrieving Solicitud_exorto by Solicitud_exortoCriteria");
		orm.Solicitud_exortoCriteria lormSolicitud_exortoCriteria = new orm.Solicitud_exortoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormSolicitud_exortoCriteria.id.eq();
		System.out.println(lormSolicitud_exortoCriteria.uniqueSolicitud_exorto());
		
		System.out.println("Retrieving Solicitud_cambio_fecha by Solicitud_cambio_fechaCriteria");
		orm.Solicitud_cambio_fechaCriteria lormSolicitud_cambio_fechaCriteria = new orm.Solicitud_cambio_fechaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormSolicitud_cambio_fechaCriteria.id.eq();
		System.out.println(lormSolicitud_cambio_fechaCriteria.uniqueSolicitud_cambio_fecha());
		
		System.out.println("Retrieving Vehiculos by VehiculosCriteria");
		orm.VehiculosCriteria lormVehiculosCriteria = new orm.VehiculosCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormVehiculosCriteria.id_vehiculo.eq();
		System.out.println(lormVehiculosCriteria.uniqueVehiculos());
		
		System.out.println("Retrieving Secretaria by SecretariaCriteria");
		orm.SecretariaCriteria lormSecretariaCriteria = new orm.SecretariaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormSecretariaCriteria.id.eq();
		System.out.println(lormSecretariaCriteria.uniqueSecretaria());
		
		System.out.println("Retrieving Juzgado by JuzgadoCriteria");
		orm.JuzgadoCriteria lormJuzgadoCriteria = new orm.JuzgadoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormJuzgadoCriteria.id.eq();
		System.out.println(lormJuzgadoCriteria.uniqueJuzgado());
		
		System.out.println("Retrieving Tesorera by TesoreraCriteria");
		orm.TesoreraCriteria lormTesoreraCriteria = new orm.TesoreraCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormTesoreraCriteria.id.eq();
		System.out.println(lormTesoreraCriteria.uniqueTesorera());
		
		System.out.println("Retrieving Infractor_persona by Infractor_personaCriteria");
		orm.Infractor_personaCriteria lormInfractor_personaCriteria = new orm.Infractor_personaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormInfractor_personaCriteria.id.eq();
		System.out.println(lormInfractor_personaCriteria.uniqueInfractor_persona());
		
		System.out.println("Retrieving Multavehiculo by MultavehiculoCriteria");
		orm.MultavehiculoCriteria lormMultavehiculoCriteria = new orm.MultavehiculoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormMultavehiculoCriteria.id.eq();
		System.out.println(lormMultavehiculoCriteria.uniqueMultavehiculo());
		
		System.out.println("Retrieving Resolucion by ResolucionCriteria");
		orm.ResolucionCriteria lormResolucionCriteria = new orm.ResolucionCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormResolucionCriteria.id.eq();
		System.out.println(lormResolucionCriteria.uniqueResolucion());
		
		System.out.println("Retrieving Pago by PagoCriteria");
		orm.PagoCriteria lormPagoCriteria = new orm.PagoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormPagoCriteria.id.eq();
		System.out.println(lormPagoCriteria.uniquePago());
		
		System.out.println("Retrieving Token by TokenCriteria");
		orm.TokenCriteria lormTokenCriteria = new orm.TokenCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormTokenCriteria.id.eq();
		System.out.println(lormTokenCriteria.uniqueToken());
		
		System.out.println("Retrieving Infractor_persona_multa by Infractor_persona_multaCriteria");
		orm.Infractor_persona_multaCriteria lormInfractor_persona_multaCriteria = new orm.Infractor_persona_multaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormInfractor_persona_multaCriteria.id.eq();
		System.out.println(lormInfractor_persona_multaCriteria.uniqueInfractor_persona_multa());
		
	}
	
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateDiagramadelaBDMULTASData retrieveAndUpdateDiagramadelaBDMULTASData = new RetrieveAndUpdateDiagramadelaBDMULTASData();
			try {
				retrieveAndUpdateDiagramadelaBDMULTASData.retrieveAndUpdateTestData();
				//retrieveAndUpdateDiagramadelaBDMULTASData.retrieveByCriteria();
			}
			finally {
				orm.DiagramadelaBDMULTASPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
