/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class ListDiagramadelaBDMULTASData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Carabinero...");
		orm.Carabinero[] ormCarabineros = orm.CarabineroDAO.listCarabineroByQuery(null, null);
		int length = Math.min(ormCarabineros.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormCarabineros[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Persona...");
		orm.Persona[] ormPersonas = orm.PersonaDAO.listPersonaByQuery(null, null);
		length = Math.min(ormPersonas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormPersonas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Multa...");
		orm.Multa[] ormMultas = orm.MultaDAO.listMultaByQuery(null, null);
		length = Math.min(ormMultas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormMultas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Solicitud_exorto...");
		orm.Solicitud_exorto[] ormSolicitud_exortos = orm.Solicitud_exortoDAO.listSolicitud_exortoByQuery(null, null);
		length = Math.min(ormSolicitud_exortos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormSolicitud_exortos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Solicitud_cambio_fecha...");
		orm.Solicitud_cambio_fecha[] ormSolicitud_cambio_fechas = orm.Solicitud_cambio_fechaDAO.listSolicitud_cambio_fechaByQuery(null, null);
		length = Math.min(ormSolicitud_cambio_fechas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormSolicitud_cambio_fechas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Vehiculos...");
		orm.Vehiculos[] ormVehiculoses = orm.VehiculosDAO.listVehiculosByQuery(null, null);
		length = Math.min(ormVehiculoses.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormVehiculoses[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Secretaria...");
		orm.Secretaria[] ormSecretarias = orm.SecretariaDAO.listSecretariaByQuery(null, null);
		length = Math.min(ormSecretarias.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormSecretarias[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Juzgado...");
		orm.Juzgado[] ormJuzgados = orm.JuzgadoDAO.listJuzgadoByQuery(null, null);
		length = Math.min(ormJuzgados.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormJuzgados[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Tesorera...");
		orm.Tesorera[] ormTesoreras = orm.TesoreraDAO.listTesoreraByQuery(null, null);
		length = Math.min(ormTesoreras.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormTesoreras[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Infractor_persona...");
		orm.Infractor_persona[] ormInfractor_personas = orm.Infractor_personaDAO.listInfractor_personaByQuery(null, null);
		length = Math.min(ormInfractor_personas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormInfractor_personas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Multavehiculo...");
		orm.Multavehiculo[] ormMultavehiculos = orm.MultavehiculoDAO.listMultavehiculoByQuery(null, null);
		length = Math.min(ormMultavehiculos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormMultavehiculos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Resolucion...");
		orm.Resolucion[] ormResolucions = orm.ResolucionDAO.listResolucionByQuery(null, null);
		length = Math.min(ormResolucions.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormResolucions[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Pago...");
		orm.Pago[] ormPagos = orm.PagoDAO.listPagoByQuery(null, null);
		length = Math.min(ormPagos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormPagos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Token...");
		orm.Token[] ormTokens = orm.TokenDAO.listTokenByQuery(null, null);
		length = Math.min(ormTokens.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormTokens[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Infractor_persona_multa...");
		orm.Infractor_persona_multa[] ormInfractor_persona_multas = orm.Infractor_persona_multaDAO.listInfractor_persona_multaByQuery(null, null);
		length = Math.min(ormInfractor_persona_multas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormInfractor_persona_multas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public void listByCriteria() throws PersistentException  {
		System.out.println("Listing Carabinero by Criteria...");
		orm.CarabineroCriteria lormCarabineroCriteria = new orm.CarabineroCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormCarabineroCriteria.id.eq();
		lormCarabineroCriteria.setMaxResults(ROW_COUNT);
		orm.Carabinero[] ormCarabineros = lormCarabineroCriteria.listCarabinero();
		int length =ormCarabineros== null ? 0 : Math.min(ormCarabineros.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormCarabineros[i]);
		}
		System.out.println(length + " Carabinero record(s) retrieved."); 
		
		System.out.println("Listing Persona by Criteria...");
		orm.PersonaCriteria lormPersonaCriteria = new orm.PersonaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormPersonaCriteria.id_usuario.eq();
		lormPersonaCriteria.setMaxResults(ROW_COUNT);
		orm.Persona[] ormPersonas = lormPersonaCriteria.listPersona();
		length =ormPersonas== null ? 0 : Math.min(ormPersonas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormPersonas[i]);
		}
		System.out.println(length + " Persona record(s) retrieved."); 
		
		System.out.println("Listing Multa by Criteria...");
		orm.MultaCriteria lormMultaCriteria = new orm.MultaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormMultaCriteria.id.eq();
		lormMultaCriteria.setMaxResults(ROW_COUNT);
		orm.Multa[] ormMultas = lormMultaCriteria.listMulta();
		length =ormMultas== null ? 0 : Math.min(ormMultas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormMultas[i]);
		}
		System.out.println(length + " Multa record(s) retrieved."); 
		
		System.out.println("Listing Solicitud_exorto by Criteria...");
		orm.Solicitud_exortoCriteria lormSolicitud_exortoCriteria = new orm.Solicitud_exortoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormSolicitud_exortoCriteria.id.eq();
		lormSolicitud_exortoCriteria.setMaxResults(ROW_COUNT);
		orm.Solicitud_exorto[] ormSolicitud_exortos = lormSolicitud_exortoCriteria.listSolicitud_exorto();
		length =ormSolicitud_exortos== null ? 0 : Math.min(ormSolicitud_exortos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormSolicitud_exortos[i]);
		}
		System.out.println(length + " Solicitud_exorto record(s) retrieved."); 
		
		System.out.println("Listing Solicitud_cambio_fecha by Criteria...");
		orm.Solicitud_cambio_fechaCriteria lormSolicitud_cambio_fechaCriteria = new orm.Solicitud_cambio_fechaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormSolicitud_cambio_fechaCriteria.id.eq();
		lormSolicitud_cambio_fechaCriteria.setMaxResults(ROW_COUNT);
		orm.Solicitud_cambio_fecha[] ormSolicitud_cambio_fechas = lormSolicitud_cambio_fechaCriteria.listSolicitud_cambio_fecha();
		length =ormSolicitud_cambio_fechas== null ? 0 : Math.min(ormSolicitud_cambio_fechas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormSolicitud_cambio_fechas[i]);
		}
		System.out.println(length + " Solicitud_cambio_fecha record(s) retrieved."); 
		
		System.out.println("Listing Vehiculos by Criteria...");
		orm.VehiculosCriteria lormVehiculosCriteria = new orm.VehiculosCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormVehiculosCriteria.id_vehiculo.eq();
		lormVehiculosCriteria.setMaxResults(ROW_COUNT);
		orm.Vehiculos[] ormVehiculoses = lormVehiculosCriteria.listVehiculos();
		length =ormVehiculoses== null ? 0 : Math.min(ormVehiculoses.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormVehiculoses[i]);
		}
		System.out.println(length + " Vehiculos record(s) retrieved."); 
		
		System.out.println("Listing Secretaria by Criteria...");
		orm.SecretariaCriteria lormSecretariaCriteria = new orm.SecretariaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormSecretariaCriteria.id.eq();
		lormSecretariaCriteria.setMaxResults(ROW_COUNT);
		orm.Secretaria[] ormSecretarias = lormSecretariaCriteria.listSecretaria();
		length =ormSecretarias== null ? 0 : Math.min(ormSecretarias.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormSecretarias[i]);
		}
		System.out.println(length + " Secretaria record(s) retrieved."); 
		
		System.out.println("Listing Juzgado by Criteria...");
		orm.JuzgadoCriteria lormJuzgadoCriteria = new orm.JuzgadoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormJuzgadoCriteria.id.eq();
		lormJuzgadoCriteria.setMaxResults(ROW_COUNT);
		orm.Juzgado[] ormJuzgados = lormJuzgadoCriteria.listJuzgado();
		length =ormJuzgados== null ? 0 : Math.min(ormJuzgados.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormJuzgados[i]);
		}
		System.out.println(length + " Juzgado record(s) retrieved."); 
		
		System.out.println("Listing Tesorera by Criteria...");
		orm.TesoreraCriteria lormTesoreraCriteria = new orm.TesoreraCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormTesoreraCriteria.id.eq();
		lormTesoreraCriteria.setMaxResults(ROW_COUNT);
		orm.Tesorera[] ormTesoreras = lormTesoreraCriteria.listTesorera();
		length =ormTesoreras== null ? 0 : Math.min(ormTesoreras.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormTesoreras[i]);
		}
		System.out.println(length + " Tesorera record(s) retrieved."); 
		
		System.out.println("Listing Infractor_persona by Criteria...");
		orm.Infractor_personaCriteria lormInfractor_personaCriteria = new orm.Infractor_personaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormInfractor_personaCriteria.id.eq();
		lormInfractor_personaCriteria.setMaxResults(ROW_COUNT);
		orm.Infractor_persona[] ormInfractor_personas = lormInfractor_personaCriteria.listInfractor_persona();
		length =ormInfractor_personas== null ? 0 : Math.min(ormInfractor_personas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormInfractor_personas[i]);
		}
		System.out.println(length + " Infractor_persona record(s) retrieved."); 
		
		System.out.println("Listing Multavehiculo by Criteria...");
		orm.MultavehiculoCriteria lormMultavehiculoCriteria = new orm.MultavehiculoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormMultavehiculoCriteria.id.eq();
		lormMultavehiculoCriteria.setMaxResults(ROW_COUNT);
		orm.Multavehiculo[] ormMultavehiculos = lormMultavehiculoCriteria.listMultavehiculo();
		length =ormMultavehiculos== null ? 0 : Math.min(ormMultavehiculos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormMultavehiculos[i]);
		}
		System.out.println(length + " Multavehiculo record(s) retrieved."); 
		
		System.out.println("Listing Resolucion by Criteria...");
		orm.ResolucionCriteria lormResolucionCriteria = new orm.ResolucionCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormResolucionCriteria.id.eq();
		lormResolucionCriteria.setMaxResults(ROW_COUNT);
		orm.Resolucion[] ormResolucions = lormResolucionCriteria.listResolucion();
		length =ormResolucions== null ? 0 : Math.min(ormResolucions.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormResolucions[i]);
		}
		System.out.println(length + " Resolucion record(s) retrieved."); 
		
		System.out.println("Listing Pago by Criteria...");
		orm.PagoCriteria lormPagoCriteria = new orm.PagoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormPagoCriteria.id.eq();
		lormPagoCriteria.setMaxResults(ROW_COUNT);
		orm.Pago[] ormPagos = lormPagoCriteria.listPago();
		length =ormPagos== null ? 0 : Math.min(ormPagos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormPagos[i]);
		}
		System.out.println(length + " Pago record(s) retrieved."); 
		
		System.out.println("Listing Token by Criteria...");
		orm.TokenCriteria lormTokenCriteria = new orm.TokenCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormTokenCriteria.id.eq();
		lormTokenCriteria.setMaxResults(ROW_COUNT);
		orm.Token[] ormTokens = lormTokenCriteria.listToken();
		length =ormTokens== null ? 0 : Math.min(ormTokens.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormTokens[i]);
		}
		System.out.println(length + " Token record(s) retrieved."); 
		
		System.out.println("Listing Infractor_persona_multa by Criteria...");
		orm.Infractor_persona_multaCriteria lormInfractor_persona_multaCriteria = new orm.Infractor_persona_multaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormInfractor_persona_multaCriteria.id.eq();
		lormInfractor_persona_multaCriteria.setMaxResults(ROW_COUNT);
		orm.Infractor_persona_multa[] ormInfractor_persona_multas = lormInfractor_persona_multaCriteria.listInfractor_persona_multa();
		length =ormInfractor_persona_multas== null ? 0 : Math.min(ormInfractor_persona_multas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormInfractor_persona_multas[i]);
		}
		System.out.println(length + " Infractor_persona_multa record(s) retrieved."); 
		
	}
	
	public static void main(String[] args) {
		try {
			ListDiagramadelaBDMULTASData listDiagramadelaBDMULTASData = new ListDiagramadelaBDMULTASData();
			try {
				listDiagramadelaBDMULTASData.listTestData();
				//listDiagramadelaBDMULTASData.listByCriteria();
			}
			finally {
				orm.DiagramadelaBDMULTASPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
