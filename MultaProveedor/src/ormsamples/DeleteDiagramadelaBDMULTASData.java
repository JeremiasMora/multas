/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class DeleteDiagramadelaBDMULTASData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Carabinero lormCarabinero = orm.CarabineroDAO.loadCarabineroByQuery(null, null);
			// Delete the persistent object
			orm.CarabineroDAO.delete(lormCarabinero);
			orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByQuery(null, null);
			// Delete the persistent object
			orm.PersonaDAO.delete(lormPersona);
			orm.Multa lormMulta = orm.MultaDAO.loadMultaByQuery(null, null);
			// Delete the persistent object
			orm.MultaDAO.delete(lormMulta);
			orm.Solicitud_exorto lormSolicitud_exorto = orm.Solicitud_exortoDAO.loadSolicitud_exortoByQuery(null, null);
			// Delete the persistent object
			orm.Solicitud_exortoDAO.delete(lormSolicitud_exorto);
			orm.Solicitud_cambio_fecha lormSolicitud_cambio_fecha = orm.Solicitud_cambio_fechaDAO.loadSolicitud_cambio_fechaByQuery(null, null);
			// Delete the persistent object
			orm.Solicitud_cambio_fechaDAO.delete(lormSolicitud_cambio_fecha);
			orm.Vehiculos lormVehiculos = orm.VehiculosDAO.loadVehiculosByQuery(null, null);
			// Delete the persistent object
			orm.VehiculosDAO.delete(lormVehiculos);
			orm.Secretaria lormSecretaria = orm.SecretariaDAO.loadSecretariaByQuery(null, null);
			// Delete the persistent object
			orm.SecretariaDAO.delete(lormSecretaria);
			orm.Juzgado lormJuzgado = orm.JuzgadoDAO.loadJuzgadoByQuery(null, null);
			// Delete the persistent object
			orm.JuzgadoDAO.delete(lormJuzgado);
			orm.Tesorera lormTesorera = orm.TesoreraDAO.loadTesoreraByQuery(null, null);
			// Delete the persistent object
			orm.TesoreraDAO.delete(lormTesorera);
			orm.Infractor_persona lormInfractor_persona = orm.Infractor_personaDAO.loadInfractor_personaByQuery(null, null);
			// Delete the persistent object
			orm.Infractor_personaDAO.delete(lormInfractor_persona);
			orm.Multavehiculo lormMultavehiculo = orm.MultavehiculoDAO.loadMultavehiculoByQuery(null, null);
			// Delete the persistent object
			orm.MultavehiculoDAO.delete(lormMultavehiculo);
			orm.Resolucion lormResolucion = orm.ResolucionDAO.loadResolucionByQuery(null, null);
			// Delete the persistent object
			orm.ResolucionDAO.delete(lormResolucion);
			orm.Pago lormPago = orm.PagoDAO.loadPagoByQuery(null, null);
			// Delete the persistent object
			orm.PagoDAO.delete(lormPago);
			orm.Token lormToken = orm.TokenDAO.loadTokenByQuery(null, null);
			// Delete the persistent object
			orm.TokenDAO.delete(lormToken);
			orm.Infractor_persona_multa lormInfractor_persona_multa = orm.Infractor_persona_multaDAO.loadInfractor_persona_multaByQuery(null, null);
			// Delete the persistent object
			orm.Infractor_persona_multaDAO.delete(lormInfractor_persona_multa);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteDiagramadelaBDMULTASData deleteDiagramadelaBDMULTASData = new DeleteDiagramadelaBDMULTASData();
			try {
				deleteDiagramadelaBDMULTASData.deleteTestData();
			}
			finally {
				orm.DiagramadelaBDMULTASPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
