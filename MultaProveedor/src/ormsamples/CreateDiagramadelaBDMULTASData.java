/**
 * Licensee: Universidad del Pais Vasco
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class CreateDiagramadelaBDMULTASData {
	public void createTestData() throws PersistentException {
		// Insert sample data
		java.sql.Connection conn = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().connection();
		try {
			java.sql.Statement statement = conn.createStatement();
			statement.executeUpdate("insert into vehiculos(id_vehiculo, marca, modelo, anio, personaid_usuario, patente) values (1, 'sin datos', 'sin datos', sin datos, null, null)");
			conn.commit();
		}
		catch (Exception e) {
			try {
				conn.rollback();
			}
			catch (java.sql.SQLException e1) {
				e.printStackTrace();
			}
			e.printStackTrace();
		}
		
		PersistentTransaction t = orm.DiagramadelaBDMULTASPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Carabinero lormCarabinero = orm.CarabineroDAO.createCarabinero();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : personaid_usuario
			orm.CarabineroDAO.save(lormCarabinero);
			orm.Persona lormPersona = orm.PersonaDAO.createPersona();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : tesorera, infractor_persona, token, carabinero, secretaria, vehiculos, juzgado, pass, rut
			orm.PersonaDAO.save(lormPersona);
			orm.Multa lormMulta = orm.MultaDAO.createMulta();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : solicitud_exorto, resolucion, pago, infractor_persona_multa, multavehiculo, solicitud_cambio_fecha, vehiculosid_vehiculo
			orm.MultaDAO.save(lormMulta);
			orm.Solicitud_exorto lormSolicitud_exorto = orm.Solicitud_exortoDAO.createSolicitud_exorto();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : multa, ciudad
			orm.Solicitud_exortoDAO.save(lormSolicitud_exorto);
			orm.Solicitud_cambio_fecha lormSolicitud_cambio_fecha = orm.Solicitud_cambio_fechaDAO.createSolicitud_cambio_fecha();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : multa
			orm.Solicitud_cambio_fechaDAO.save(lormSolicitud_cambio_fecha);
			orm.Vehiculos lormVehiculos = orm.VehiculosDAO.createVehiculos();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : multavehiculo, multa, patente, personaid_usuario
			orm.VehiculosDAO.save(lormVehiculos);
			orm.Secretaria lormSecretaria = orm.SecretariaDAO.createSecretaria();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : personaid_usuario
			orm.SecretariaDAO.save(lormSecretaria);
			orm.Juzgado lormJuzgado = orm.JuzgadoDAO.createJuzgado();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : resolucion, personaid_usuario
			orm.JuzgadoDAO.save(lormJuzgado);
			orm.Tesorera lormTesorera = orm.TesoreraDAO.createTesorera();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : personaid_usuario
			orm.TesoreraDAO.save(lormTesorera);
			orm.Infractor_persona lormInfractor_persona = orm.Infractor_personaDAO.createInfractor_persona();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : infractor_persona_multa, personaid_usuario
			orm.Infractor_personaDAO.save(lormInfractor_persona);
			orm.Multavehiculo lormMultavehiculo = orm.MultavehiculoDAO.createMultavehiculo();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : multa, vehiculosid_vehiculo
			orm.MultavehiculoDAO.save(lormMultavehiculo);
			orm.Resolucion lormResolucion = orm.ResolucionDAO.createResolucion();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : multa, juzgado
			orm.ResolucionDAO.save(lormResolucion);
			orm.Pago lormPago = orm.PagoDAO.createPago();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : multa, formapago
			orm.PagoDAO.save(lormPago);
			orm.Token lormToken = orm.TokenDAO.createToken();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : personaid_usuario
			orm.TokenDAO.save(lormToken);
			orm.Infractor_persona_multa lormInfractor_persona_multa = orm.Infractor_persona_multaDAO.createInfractor_persona_multa();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : multa, infractor_persona
			orm.Infractor_persona_multaDAO.save(lormInfractor_persona_multa);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateDiagramadelaBDMULTASData createDiagramadelaBDMULTASData = new CreateDiagramadelaBDMULTASData();
			try {
				createDiagramadelaBDMULTASData.createTestData();
			}
			finally {
				orm.DiagramadelaBDMULTASPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
