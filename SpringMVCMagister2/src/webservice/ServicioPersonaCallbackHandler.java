
/**
 * ServicioPersonaCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package webservice;

    /**
     *  ServicioPersonaCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ServicioPersonaCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ServicioPersonaCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ServicioPersonaCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for loguearse method
            * override this method for handling normal response from loguearse operation
            */
           public void receiveResultloguearse(
                    webservice.ServicioPersonaStub.LoguearseResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from loguearse operation
           */
            public void receiveErrorloguearse(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for crearPersona method
            * override this method for handling normal response from crearPersona operation
            */
           public void receiveResultcrearPersona(
                    webservice.ServicioPersonaStub.CrearPersonaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from crearPersona operation
           */
            public void receiveErrorcrearPersona(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for mostrarPersona method
            * override this method for handling normal response from mostrarPersona operation
            */
           public void receiveResultmostrarPersona(
                    webservice.ServicioPersonaStub.MostrarPersonaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from mostrarPersona operation
           */
            public void receiveErrormostrarPersona(java.lang.Exception e) {
            }
                


    }
    