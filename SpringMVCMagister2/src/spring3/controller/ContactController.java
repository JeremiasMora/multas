package spring3.controller;

//import net.viralpatel.spring3.form.Contact;
import java.rmi.RemoteException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import spring3.form.Contact;
import spring3.form.ContactForm;
import spring3.form.Multa;
import spring3.form.Vehiculos;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import webservice.ServicioBuscarInfractorStub;
import webservice.ServicioBuscarInfractorStub.BuscarInfractorResponse;
import webservice.ServicioBuscarVehiculoStub;
import webservice.ServicioCrearMultaStub;
import webservice.ServicioInfractorStub;
import webservice.ServicioInfractorStub.CrearInfractor;
import webservice.ServicioInfractorStub.CrearInfractorResponse;
import webservice.ServicioPersonaStub;
import webservice.ServicioPersonaStub.LoginVO;
import webservice.ServicioPersonaStub.Loguearse;
import webservice.ServicioPersonaStub.LoguearseResponse;
import webservice.ServicioPersonaStub.MostrarPersona;
import webservice.ServicioPersonaStub.MostrarPersonaResponse;
import webservice.ServicioPersonaStub.Persona;

// TODO: Auto-generated Javadoc
/**
 * The Class ContactController.
 */
@Controller
@SessionAttributes
public class ContactController {

	/**
	 * Adds the contact.
	 * @param contact the contact
	 * @param result the result
	 * @return the model and view
	 */
	@RequestMapping(value = "/addContact", method = RequestMethod.POST)
	public ModelAndView addContact(@ModelAttribute("contact") @Valid  Contact contact, BindingResult result, HttpSession session) {
		
		String valor = (String) session.getAttribute("token");
   	 if(result.hasErrors()) {
   		 System.out.println("ERROR");
            return new ModelAndView("contact");
        }
   	 if (valor != null) {
   		 System.out.print("HAY VALOR DE SESION");
		if (valor.equals("logueado")) {
			System.out.print("NO ESTAS LOGEADO");
			return new ModelAndView("informacion", "message", new Contact());
		}else {
			System.out.println("sesi�n expirada");
			return new ModelAndView("Formlogin", "command", new spring3.form.Login());
		}
   	 }else {
   		System.out.print("NO HAY VALOR DE SESION");
			try {
				ServicioInfractorStub iStub = new ServicioInfractorStub();
				
				webservice.ServicioInfractorStub.Persona pers = new webservice.ServicioInfractorStub.Persona();
				
				// AGREGA
				pers.setNombre(contact.getNombre());
				pers.setApellido(contact.getApellido());
				pers.setDireccion(contact.getDireccion());
				pers.setPass(contact.getPass());
				pers.setRut(contact.getRut());
				
				CrearInfractor crearinfractor = new CrearInfractor();
				crearinfractor.setP(pers);
				
				CrearInfractorResponse objResponseInfractor = iStub.crearInfractor(crearinfractor);
				String mensaje = objResponseInfractor.get_return();


				System.out.println("spring "+mensaje);
				if(contact.getEsllamadocrearmulta()!=null && contact.getEsllamadocrearmulta().equals("1")){
					System.out.println("spring "+mensaje);
					//todo falta crearlo como infractor
					Multa obj = new Multa();
					obj.setIdinfractor(""+mensaje);
					return new ModelAndView("multa", "command", obj);
				}
				else {
					return new ModelAndView("informacion", "message", mensaje);
				}
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				return new ModelAndView("error", "message", "ERROR");
			}
		}
	}
	//------------------------------------------------------------------------
	
	/** 
	 * Buscar infractor.
	 *
	 * @param contact the contact
	 * @param result the result
	 * @return the model and view
	 */
	@RequestMapping(value = "/buscarInfractor", method = RequestMethod.POST)
	public ModelAndView BuscarInfractor(@ModelAttribute("contact") @Valid  Contact contact ,BindingResult result) {
        //BindingResult result, 
   	 if(result.hasErrors()) {
   		 System.out.println("ERROR");
            return new ModelAndView("multa");
        }
       System.out.println("Rut:" + contact.getRut());


		try {

			ServicioBuscarInfractorStub iStub = new ServicioBuscarInfractorStub();		
			webservice.ServicioBuscarInfractorStub.Persona persVO = new webservice.ServicioBuscarInfractorStub.Persona();

			
			// Buscar por rut
			persVO.setRut(contact.getRut());
			
			webservice.ServicioBuscarInfractorStub.BuscarInfractor buscarInfractor = new webservice.ServicioBuscarInfractorStub.BuscarInfractor();
			buscarInfractor.setP(persVO);
			
			BuscarInfractorResponse objResponseMulta = iStub.buscarInfractor(buscarInfractor);
			int mensaje = objResponseMulta.get_return();
			System.out.println("MENSAJE "+mensaje);
			if(mensaje==0){
				System.out.println("spring "+mensaje);
				 Contact obj = new Contact();
				 obj.setEsllamadocrearmulta("1");
				return new ModelAndView("contact_2", "command", obj);
			}
			else{
				System.out.println("spring "+mensaje);
				Multa obj = new Multa();
				obj.setIdinfractor(""+mensaje);
				return new ModelAndView("multa", "command", obj);
			}
			
			
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return new ModelAndView("error", "message", "ERROR");
		}

	}
	//------------------------------------------------------------------
	
		/**
	 * Adds the multa.
	 *
	 * @param multa the multa
	 * @param result the result
	 * @return the model and view
	 */
	@RequestMapping(value = "/addMulta", method = RequestMethod.POST)
		public ModelAndView addMulta(@ModelAttribute("multa") @Valid  Multa multa,BindingResult result) {
	        //BindingResult result, 
	   	 if(result.hasErrors()) {
	   		 System.out.println("ERROR");
	            return new ModelAndView("multa");
	        }
	   //	DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); // "dd/MM/yyyy HH:mm:ss"
		int mes = Calendar.getInstance().getTime().getMonth()+1;
		int dia = Calendar.getInstance().getTime().getDay();
		int anio = Calendar.getInstance().getTime().getYear()-100;
		String date = dia + "/" + mes+"/"+anio;

	       System.out.println("Nombre:" + multa.getNombre_multa()+ "\n"+
	               "Descripcion:" + multa.getDescripcion_multa()+"\n"+
	               "Fecha:" + multa.getFecha_creacion()+"\n"+
	               "Quien Modifico:" + multa.getQuien_modifico_multa()+"\n");

			try {

				ServicioCrearMultaStub iStub = new ServicioCrearMultaStub();
				
				webservice.ServicioCrearMultaStub.Multa multaVO = new webservice.ServicioCrearMultaStub.Multa();
				
				multaVO.setNombre_multa(multa.getNombre_multa());
				multaVO.setDescripcion_multa(multa.getDescripcion_multa());
				//multaVO.setFecha_creacion(multa.getFecha_creacion());
				multaVO.setFecha_creacion(date);
				multaVO.setQuien_modifico_multa(multa.getQuien_modifico_multa());
				multaVO.setQuien_registro_multa(multa.getQuien_registro_multa());
				
				// AGREGA
				System.out.println("ID INFRACTOR"+multa.getIdinfractor());
				webservice.ServicioCrearMultaStub.CrearMulta crearMulta = new webservice.ServicioCrearMultaStub.CrearMulta();
				crearMulta.setM(multaVO);
				crearMulta.setIdInfractor(Integer.parseInt(multa.getIdinfractor()));
				
				webservice.ServicioCrearMultaStub.CrearMultaResponse objResponseMulta = iStub.crearMulta(crearMulta);
				String mensaje = objResponseMulta.get_return();
				

				
				System.out.println("spring "+mensaje);
				return new ModelAndView("informacion", "message", mensaje);
				
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				return new ModelAndView("error", "message", "ERROR");
			}

		}
	//-----------------------------------------------------
		/**
	 * Search vehiculo.
	 *
	 * @param vehiculo the vehiculo
	 * @param result the result
	 * @return the model and view
	 */
	@RequestMapping(value = "/searchVehiculo", method = RequestMethod.POST)
		public ModelAndView searchVehiculo(@ModelAttribute("vehiculo") @Valid  Vehiculos vehiculo, BindingResult result) {
	        //BindingResult result, 
	   	 if(result.hasErrors()) {
	   		 System.out.println("ERROR");
	            return new ModelAndView("vehiculo");
	        }
	   	 		System.out.println("Patente:" + vehiculo.getPatente());

			try {
				ServicioBuscarVehiculoStub iStub = new ServicioBuscarVehiculoStub();
						
				ServicioBuscarVehiculoStub.MostrarVehiculo mostrarVehiculo = new webservice.ServicioBuscarVehiculoStub.MostrarVehiculo();
				mostrarVehiculo.setPatente(vehiculo.getPatente());
				
				webservice.ServicioBuscarVehiculoStub.MostrarVehiculoResponse objResponseVehiculo = iStub.mostrarVehiculo(mostrarVehiculo);
				webservice.ServicioBuscarVehiculoStub.Vehiculos mensaje = objResponseVehiculo.get_return();
				System.out.println("Spring:" + mensaje.getNombre_duenio());
				
				return new ModelAndView("info_vehiculo","command", mensaje);
				
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				return new ModelAndView("error", "message", "ERROR");
			}

		}
		

		
	//-------------------------------------------------------------------------------

	/**
	 * Form vehiculo.
	 *
	 * @return the model and view
	 */
	@RequestMapping("/form-vehiculo")
	public ModelAndView formVehiculo() {

		return new ModelAndView("searchVehiculo", "command", new Vehiculos());
	}
	
	/**
	 * Informacion.
	 *
	 * @return the model and view
	 */
	@RequestMapping("/informacion")
	public ModelAndView informacion() {

		return new ModelAndView("informacion", "command", new Contact());
	}

	/**
	 * Show contacts.
	 *
	 * @return the model and view
	 */
	@RequestMapping("/contacts")
	public ModelAndView showContacts() {

		return new ModelAndView("contact", "command", new Contact());
	}
	
	/**
	 * Show infractor.
	 *
	 * @return the model and view
	 */
	@RequestMapping("/buscar_infractor")
	public ModelAndView showInfractor() {

		return new ModelAndView("buscar_infractor", "command", new Contact());
	}

	//------------------------------------------------------------------------------
	
	/**
	 * Hello world.
	 *
	 * @return the model and view
	 */
	@RequestMapping("/show")
	public ModelAndView helloWorld() {
		// String message = "Hola Mundo Spring se ha instaldo correctamente";
		List<Contact> contacts = new ArrayList<Contact>();

		ContactForm contactForm = new ContactForm();

		try {

			ServicioPersonaStub oStub = new ServicioPersonaStub();
			MostrarPersona oMostrarContacto = new MostrarPersona();

			MostrarPersonaResponse objResponde = oStub.mostrarPersona(oMostrarContacto);
			Persona[] contacts2 = objResponde.get_return();

			contactForm.setContacts(contacts2);
			return new ModelAndView("showcontactrow", "contactForm",
					contactForm);

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return new ModelAndView("error", "message", "ERROR");
		}
	}
	@RequestMapping("/login")
	public ModelAndView login(
			@ModelAttribute("login") @Valid spring3.form.Login login,
			BindingResult result, HttpSession session) {

		if (result.hasErrors()) {
			System.out.println("ERROR");
			return new ModelAndView("Formlogin");
		}

		try {
			System.out.println("FORMULARIO "+login.getRut());
			ServicioPersonaStub oStubU = new ServicioPersonaStub();
			
			LoginVO oLoginVO = new LoginVO();
			oLoginVO.setRut(login.getRut());
			oLoginVO.setPass(login.getPass());
			
			Loguearse oLoguearse = new Loguearse();
			oLoguearse.setUser(oLoginVO);
			
			LoguearseResponse objResponse = oStubU.loguearse(oLoguearse);
			boolean datosUsuario = objResponse.get_return();

			if (datosUsuario) {
				session.setAttribute("token", "logueado");
				return new ModelAndView("informacion", "command" , new Contact());
			} else {
				// return new ModelAndView("error");
				return new ModelAndView("Formlogin", "command", new spring3.form.Login());
			}
		} catch (RemoteException e) {
			e.printStackTrace();
			return new ModelAndView("Formlogin", "command", new spring3.form.Login());
		}
	}
}