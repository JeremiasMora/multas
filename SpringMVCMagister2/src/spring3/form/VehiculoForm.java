package spring3.form;

public class VehiculoForm  implements java.io.Serializable {
    private java.lang.Object vehiculo;

    public VehiculoForm() {
    }

    public VehiculoForm(java.lang.Object vehiculo) {
           this.vehiculo = vehiculo;
    }


    /**
     * Gets the contacts value for this ContactForm.
     * 
     * @return contacts
     */
    public java.lang.Object getVehiculo() {
        return vehiculo;
    }


    /**
     * Sets the contacts value for this ContactForm.
     * 
     * @param contacts
     */
    public void setVehiculo(java.lang.Object vehiculo) {
        this.vehiculo = vehiculo;
    }


}
