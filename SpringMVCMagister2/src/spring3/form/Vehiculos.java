package spring3.form;

public class Vehiculos  implements java.io.Serializable{

	private java.lang.String marca;
	private java.lang.String modelo;
	private java.lang.String nombre_duenio;
	private int anio;
	private java.lang.String patente;
	private java.lang.String rut_duenio;
	
	
	public java.lang.String getRut_duenio() {
		return rut_duenio;
	}
	public void setRut_duenio(java.lang.String rut_duenio) {
		this.rut_duenio = rut_duenio;
	}
	public Vehiculos(String marca, String modelo, String nombre_duenio,
			int anio, String patente) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.nombre_duenio = nombre_duenio;
		this.anio = anio;
		this.patente = patente;
	}
	public Vehiculos (){
		
	}
	public java.lang.String getMarca() {
		return marca;
	}
	public void setMarca(java.lang.String marca) {
		this.marca = marca;
	}
	public java.lang.String getModelo() {
		return modelo;
	}
	public void setModelo(java.lang.String modelo) {
		this.modelo = modelo;
	}
	public java.lang.String getNombre_duenio() {
		return nombre_duenio;
	}
	public void setNombre_duenio(java.lang.String nombre_duenio) {
		this.nombre_duenio = nombre_duenio;
	}
	public int getAnio() {
		return anio;
	}
	public java.lang.String getPatente() {
		return patente;
	}
	public void setPatente(java.lang.String patente) {
		this.patente = patente;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	
}
