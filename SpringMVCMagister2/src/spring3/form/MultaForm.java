package spring3.form;

public class MultaForm implements java.io.Serializable {
	private java.lang.Object multas;
	
	public MultaForm() {
    }
	
	public MultaForm(
	           java.lang.Object multas) {
	           this.multas = multas;
	}

	public java.lang.Object getMultas() {
		return multas;
	}

	public void setMultas(java.lang.Object multas) {
		this.multas = multas;
	}
	
	
}
