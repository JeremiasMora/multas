package spring3.form;

public class Login {
	
	private java.lang.String rut;
	
	private java.lang.String pass;

	public Login() {
		super();
	}

	public Login(String rut, String pass) {
		super();
		this.rut = rut;
		this.pass = pass;
	}

	public java.lang.String getRut() {
		return rut;
	}

	public void setMail(java.lang.String rut) {
		this.rut = rut;
	}

	public java.lang.String getPass() {
		return pass;
	}

	public void setPass(java.lang.String pass) {
		this.pass = pass;
	}
	
	

}