/**
 * Contact.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package spring3.form;

public class Contact  implements java.io.Serializable {
	 
    private java.lang.String nombre;
  
    private java.lang.String apellido;
 
    private java.lang.String direccion;
    
    private java.lang.String pass;
    
    private java.lang.String rut;
    private java.lang.String esllamadocrearmulta;
    
    
    public java.lang.String getEsllamadocrearmulta() {
		return esllamadocrearmulta;
	}

	public void setEsllamadocrearmulta(java.lang.String esllamadocrearmulta) {
		this.esllamadocrearmulta = esllamadocrearmulta;
	}

	public Contact() {
    }

    public Contact(
           java.lang.String nombre,
           java.lang.String apellido,
           java.lang.String direccion,
           java.lang.String pass,
           java.lang.String rut) {
           this.nombre = nombre;
           this.apellido = apellido;
           this.direccion = direccion;
           this.pass = pass;
           this.rut = rut;
    }

	public java.lang.String getNombre() {
		return nombre;
	}

	public void setNombre(java.lang.String nombre) {
		this.nombre = nombre;
	}

	public java.lang.String getApellido() {
		return apellido;
	}

	public void setApellido(java.lang.String apellido) {
		this.apellido = apellido;
	}

	public java.lang.String getDireccion() {
		return direccion;
	}

	public void setDireccion(java.lang.String direccion) {
		this.direccion = direccion;
	}

	public java.lang.String getPass() {
		return pass;
	}

	public void setPass(java.lang.String pass) {
		this.pass = pass;
	}

	public java.lang.String getRut() {
		return rut;
	}

	public void setRut(java.lang.String rut) {
		this.rut = rut;
	}


    /**
     * Gets the email value for this Contact.
     * 
     * @return email
     */
  


    /**
     * Gets the telephone value for this Contact.
     * 
     * @return telephone
     */


}
