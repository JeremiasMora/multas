/**
 * Multa.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package spring3.form;

public class Multa  implements java.io.Serializable{
	private java.lang.String nombre_multa;
	private java.lang.String descripcion_multa;
	private java.lang.String quien_registro_multa;
	private java.lang.String quien_modifico_multa;
	private java.lang.String fecha_creacion;
	private java.lang.Character desactivar;
	private java.lang.String idinfractor;
	
	public java.lang.String getIdinfractor() {
		return idinfractor;
	}
	public void setIdinfractor(java.lang.String idinfractor) {
		this.idinfractor = idinfractor;
	}
	public Multa(){
		
	}
	public Multa(
	           java.lang.String nombre_multa,
	           java.lang.String descripcion_multa,
	           java.lang.String quien_registro_multa,
	           java.lang.String quien_modifico_multa,
	           java.lang.String fecha_creacion,
	           java.lang.Character desactivar) {
	           this.nombre_multa = nombre_multa;
	           this.descripcion_multa = descripcion_multa;
	           this.quien_registro_multa = quien_registro_multa;
	           this.quien_modifico_multa = quien_modifico_multa;
	           this.fecha_creacion = fecha_creacion;
	           this.desactivar = desactivar;
	    }

	public java.lang.String getNombre_multa() {
		return nombre_multa;
	}

	public void setNombre_multa(java.lang.String nombre_multa) {
		this.nombre_multa = nombre_multa;
	}

	public java.lang.String getDescripcion_multa() {
		return descripcion_multa;
	}

	public void setDescripcion_multa(java.lang.String descripcion_multa) {
		this.descripcion_multa = descripcion_multa;
	}

	public java.lang.String getQuien_registro_multa() {
		return quien_registro_multa;
	}

	public void setQuien_registro_multa(java.lang.String quien_registro_multa) {
		this.quien_registro_multa = quien_registro_multa;
	}

	public java.lang.String getQuien_modifico_multa() {
		return quien_modifico_multa;
	}

	public void setQuien_modifico_multa(java.lang.String quien_modifico_multa) {
		this.quien_modifico_multa = quien_modifico_multa;
	}

	public java.lang.String getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(java.lang.String fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public java.lang.Character getDesactivar() {
		return desactivar;
	}

	public void setDesactivar(java.lang.Character desactivar) {
		this.desactivar = desactivar;
	}
	
	
}
