<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="./resorces/css/bootstrap.css" rel="stylesheet">
</head>
<body>
<br><br>
<div class="col-md-8">
	<h2>Buscar persona</h2>
		<form:form method="post" action="buscarInfractor.html">
 			<table class="form-horizontal">
    			<tr>
        			<td><form:label path="rut"><spring:message code="label.rut"/></form:label></td>
        			<td><form:input path="rut" class="form-control" /> <form:errors></form:errors></td> 
    			</tr>
    			<tr>
        			<td colspan="2">
            			<input type="submit" value="Buscar"/>
        			</td>
   	 			</tr>
			</table>  
		</form:form>
</div>
</body>
</html>