<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Ejemplo de Resultado en Tabla</title>
    <link href="./resorces/css/bootstrap.css" rel="stylesheet">

</head>
<body>
<h2>Registros Ingresados</h2>
<table width="50%" class="table">
    <tr>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Direccion</th>
        <th>Rut</th>
    </tr>
    <c:forEach items="${contactForm.contacts}" var="contact" varStatus="status">
        <tr>
            <td>${contact.nombre}</td>
            <td>${contact.apellido}</td>
            <td>${contact.direccion}</td>
            <td>${contact.rut}</td>
        </tr>
    </c:forEach>
</table>  
<br/>
<input type="button" class="btn btn-success" value="Back" onclick="javascript:history.back()"/>
<script src="./resources/js/bootstrap.js"></script>
</body>
</html>