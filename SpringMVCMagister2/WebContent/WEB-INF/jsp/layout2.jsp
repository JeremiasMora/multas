<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="Multa PDS">
<meta name="author" content="">
<link href="./resources/css/bootstrap.css" rel="stylesheet">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>
<body>

	<div>
		<tiles:insertAttribute name="header" />
	</div>
	
	<div class="container-fluid">
		<div class="row">
			<div>
				<tiles:insertAttribute name="body" />
			</div>
		</div>
	</div>
	
	<script src="./resources/js/jquery.js"></script>
	<script src="./resources/js/bootstrap.js"></script>

</body>
</html>