<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
    <title>Registro de Contacto</title>
     <link href="./resorces/css/bootstrap.css" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
      <div class="row">
      	<div class="col-md-4"></div>
      	<div class="col-md-4"></div>
      	<div class="col-md-4">
      	<br><br>
<h2>Cursar multa</h2>

<form:form method="post" action="addMulta.html" >
<form:hidden path="idinfractor" />
    <table class="form-horizontal">
    <tr class="form-group">
        <td><form:label path="nombre_multa"><spring:message code="label.nombre_multa"/></form:label></td>
        <td><form:input path="nombre_multa" class="form-control"/> <form:errors></form:errors></td> 
    </tr>
    <tr class="form-group">
        <td><form:label path="descripcion_multa"><spring:message code="label.descripcion_multa"/></form:label></td>
        <td><form:input path="descripcion_multa" class="form-control" /></td>
    </tr>
    <tr class="form-group">
        <td><form:label path="quien_registro_multa"><spring:message code="label.quien_registro_multa"/></form:label></td>
        <td><form:input path="quien_registro_multa" class="form-control" /></td>
    </tr>
    <!-- 	
    <tr class="form-group">
        <td><form:label path="fecha_creacion"><spring:message code="label.fecha_creacion"/></form:label></td>
        <td><form:input path="fecha_creacion" class="form-control"/></td>
    </tr> -->
    <tr>
        <td colspan="2">
            <input type="submit" value="Multar"/>
        </td>
    </tr>
</table>  
     
</form:form>
</div>
      </div>
    </div> 
</body>
</html>