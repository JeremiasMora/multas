<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
    <title>Registro de Contacto</title>
     <link href="./resorces/css/bootstrap.css" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
      <div class="row">
      	<div class="col-md-4"></div>
      	<div class="col-md-4"></div>
      	<div class="col-md-4">
<h2>Registrar Infractor</h2>

<form:form method="post" action="addContact.html" >

<form:hidden path="esllamadocrearmulta" />
    <table class="form-horizontal">
    <tr class="form-group">
        <td><form:label path="nombre"><spring:message code="label.nombre"/></form:label></td>
        <td><form:input path="nombre" class="form-control"/> <form:errors></form:errors></td> 
    </tr>
    <tr class="form-group">
        <td><form:label path="apellido"><spring:message code="label.apellido"/></form:label></td>
        <td><form:input path="apellido" class="form-control" /></td>
    </tr>
    <tr class="form-group">
        <td><form:label path="direccion"><spring:message code="label.direccion"/></form:label></td>
        <td><form:input path="direccion" class="form-control" /></td>
    </tr>
    <tr class="form-group">
        <td><form:label path="pass"><spring:message code="label.pass"/></form:label></td>
        <td><form:input path="pass" class="form-control"/></td>
    </tr>
     <tr class="form-group">
        <td><form:label path="rut"><spring:message code="label.rut"/></form:label></td>
        <td><form:input path="rut" class="form-control"/></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Add Persona"/>
        </td>
    </tr>
</table>  
     
</form:form>
</div>
      </div>
    </div> 
</body>
</html>