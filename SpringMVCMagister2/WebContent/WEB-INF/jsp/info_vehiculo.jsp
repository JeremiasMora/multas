<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Ejemplo de Resultado en Tabla</title>
    <link href="./resorces/css/bootstrap.css" rel="stylesheet">

</head>
<body>
<br><br>
<h2>Informaci�n del Vehiculo</h2>
			<div class="col-md-8">
				<br><br>
				<div class="well">
				<table width="50%" class="table">
				    <tr>
				        <th>Marca</th>
				        <th>Modelo</th>
				        <th>Patente</th>
				        <th>a�oo</th>
				        <th>Due�o</th>
				        <th>Rut due�o</th>
				    </tr>
				
				        <tr>
				            <td>${command.marca}</td>
				            <td>${command.modelo}</td>
				            <td>${command.patente}</td>
				            <td>${command.anio}</td>
				            <td>${command.nombre_duenio}</td>
				            <td>${command.rut_duenio}</td>
				        </tr>
				  	
				 </table> 
				</div>
			</div>
<br/>
<script src="./resources/js/bootstrap.js"></script>
</body>
</html>