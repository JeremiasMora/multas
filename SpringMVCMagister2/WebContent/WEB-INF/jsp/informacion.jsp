<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Ejemplo de Resultado en Tabla</title>
    <link href="./resorces/css/bootstrap.css" rel="stylesheet">

</head>
<body>
<br><br>

			<div class="col-md-8">
			<h2>Información personal</h2>
				<br><br>
				<div class="well">
					<div class="">
						<p class="lead">Nombre:<span class="negrita"> ${contact.nombre} </span></p>
						<p class="lead">Apellido:<span class="negrita"> ${contact.apellido} </span></p>
						<p class="lead">Rut:<span class="negrita">${contact.rut}</span></p>
						<p class="lead">Dirección:<span class="negrita">${contact.direccion}</span></p>
						<p class="lead">Cantidad de infracciónes cometidas:<span class="negrita"> #3</span></p>
					</div>
				</div>
			</div>
<br/>
<script src="./resources/js/bootstrap.js"></script>
</body>
</html>