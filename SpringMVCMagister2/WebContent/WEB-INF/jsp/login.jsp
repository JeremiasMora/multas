<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta name="description" content="my city PDS">
<meta name="author" content="">
<link href="./resorces/css/bootstrap.css" rel="stylesheet">
</head>
<body>
	<div class="col-md-offset-1 main">
		<div class="well">
			<h2 align="center" class="form-signin-heading">
				<spring:message code="label.login" />
			</h2>
			<form:form class="form-signin" role="form" method="post" action="login.html">
				<div class="controls">
					<form:input path="rut" type="username" class="form-control"
						placeholder="Correo electrónico" />
					<form:errors></form:errors>
					<br>
					<form:input path="pass" class="form-control"
						placeholder="Contraseńa" />
					<br> <br>
					<button class="btn btn-lg btn-primary btn-block" type="submit">
						<spring:message code="label.login" />
					</button>
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>