<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
    <title>Registro de Contacto</title>
     <link href="./resorces/css/bootstrap.css" rel="stylesheet">
</head>

<body>
  	<div id="font">
  		<img src="./resorces/img/images.jpg" alt="Foto fondo">
  	</div>
<div class="container-fluid">
      <div class="row">
      	<div class="col-md-4"></div>
      	<div class="col-md-4"></div>
      	<div class="col-md-4">
<br><br>
<h2>MULTAS</h2>
<form:form method="post" action="addContact.html" >
<br><br>
<form:hidden path="esllamadocrearmulta" />
    <table class="form-horizontal col-lg-10">
    <tr class="form-group">
    	
       <!-- <td><form:label path="nombre"><spring:message code="label.nombre"/></form:label></td>-->
        <td><form:input path="nombre" placeholder="Nombre" class="form-control"/> <form:errors></form:errors><br></td> 
    </tr>
    <tr class="form-group">
        <!--<td><form:label path="apellido"><spring:message code="label.apellido"/></form:label></td>-->
        <td><form:input path="apellido"  placeholder="Apellido" class="form-control" /><br></td>
    </tr>
    <tr class="form-group">
        <!--<td><form:label path="direccion"><spring:message code="label.direccion"/></form:label></td>-->
        <td><form:input path="direccion" placeholder="Direcci�n" class="form-control" /><br></td>
    </tr>
    <tr class="form-group">
        <!--<td><form:label path="pass"><spring:message code="label.pass"/></form:label></td>-->
        <td><form:input path="pass" placeholder="Contrase�a" class="form-control"/><br></td>
    </tr>
     <tr class="form-group">
        <!--<td><form:label path="rut"><spring:message code="label.rut"/></form:label></td>-->
        <td><form:input path="rut" placeholder="Rut-ejemplo: 183035649" class="form-control"/><br></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Registrar" class="btn btn-primary btn-lg btn-block"/>
        </td>
    </tr>
</table>  
     
</form:form>
</div>
      </div>
    </div> 
</body>
</html>